#if defined _config_included
	#endinput
#endif

#define _config_included

#include <amxmodx>
#include <cstrike>

//new const slevel_up[] = "CSRank/level_up.wav"
//new const smedal_up[] = "CSRank/medal_up.wav"

new const _INI_CHANCE_DROP_TIME = 4;

new const itemChances[] = {
	1,
	3,
	15,
	100
}

public CalculateDropTimeChance(id) {
	new frags = get_user_frags(id);
	new deaths = cs_get_user_deaths(id);

	new extraChance = 0;

	if ( frags <= deaths ) 	extraChance = 0;
	else					extraChance = random_num(1,100) <= random_num(1, frags-deaths) ? random_num(0,getMedalExtraChance(id) * 2)  : 0;

	return random_num(1, _INI_CHANCE_DROP_TIME + extraChance)
}

public CalculateItemChance(id, &chance) {
	new _item_random = random_num(1, 100);
	chance = _item_random;

	new extraChance = random_num(0, getMedalExtraChance(id))

	for(new i; i< sizeof(itemChances); i++) {
		if ( _item_random <= random_num(itemChances[i], itemChances[i] + extraChance))
			return 4 - i;
	}

	return 1;

}

stock CalculateSmartItem(id, item = 4) { 
	return random_num(itemChances[item-1], itemChances[item-1] + random_num(0,getMedalExtraChance(id)));
}

stock getMedalExtraChance(id) {
	new medal = 0;
	new curr_medal = g_player_stats[id][MEDAL];

	new medalPerLvL = 5;

	if ( curr_medal > 15 ) medalPerLvL = 6;
	else if (curr_medal >= 20) medalPerLvL = 7;

	while ( curr_medal > 0 ) {
		if (curr_medal < medalPerLvL) break;

		curr_medal -= medalPerLvL;
		medal += 1;
	}

	return medal;
}