#if defined _comm_included
	#endinput
#endif

#define _comm_included

#include <amxmodx>
#include <sqlx>

public CSRAddFeature(id)
{
	if( id && !has_access(id, g_Cvars[CVAR_MAIN_FLAG]))
		return console_print(id, "[%s] Access denied", PLUGIN)
		
	new player[32], bonus[32], value[32];

	read_argv(1, player, sizeof(player) - 1);
	read_argv(2, bonus, sizeof(bonus) - 1);
	read_argv(3, value, sizeof(value) - 1);
	
	new target = cmd_target(id, player, CMDTARGET_NO_BOTS);
	if( !target ) return console_print(id, "[%s] Can't find player with '%s'", PLUGIN, player);

	static const args[][] = {
		"exp",
		"coin",
		"case",
		"key",
		"wish",
		"skin_id",
		"smart_id"
	}

	new type = -1;

	for(new i; i< sizeof(args); i++) {
		if ( equali( bonus, args[i]) ) {
			type = i;
			break;
		}
	}
	if( type < 0)
		return console_print(id, "[%s] Invalid argument <2>(type) '%s'", PLUGIN, bonus);

	new count = str_to_num(value);
	if( count < 0 )
		return console_print(id, "[%s] Invalid argument <3>(count) '%s'", PLUGIN, value);
	
	return PLUGIN_HANDLED;
}

public ShowRank(id)
{
	new query[512], Data[1]; Data[0] = id;

	formatex(query, charsmax(query), "SELECT dummy1.row_number FROM ( SELECT @id := @id + 1 AS row_number, `%s`.* FROM `%s`, (SELECT @id := 0) AS dummy0 ORDER BY `%s`.`player_medal` DESC ) AS dummy1 WHERE (dummy1.`player_id` = '%s' OR dummy1.`player_ip`= '%s')",
		TABLE,TABLE,TABLE,g_player_data[id][PD_AUTHID], g_player_data[id][PD_IP])

	SQL_ThreadQuery(g_Sqlx, "QueryLoadRank", query, Data, 1)
	
}

public QueryLoadRank(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}

	if(SQL_NumResults(Query) < 1)    
		return SQL_FreeHandle(Query);
	
	new id = Data[0];
	new rank = SQL_ReadResult(Query, 0) + 0;

	Print(id, "%L", id, "CSRANK_CS_SHOW_RANK", rank, g_player_stats[id][EXP], g_player_stats[id][MEDAL])
	return SQL_FreeHandle(Query);
}

