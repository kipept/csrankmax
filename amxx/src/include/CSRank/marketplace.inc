#if defined _mp_included
	#endinput
#endif

#define _mp_included

#include <amxmodx>
#include <sqlx>

new bool:EnableMarket = false;

MarketPlace_init()
{
	g_marketplace_offers = ArrayCreate(mpArray);
}
AddToMarketplace(id)
{
	if (!EnableMarket ) return Print(id,"This feature disabled")

	if( g_marketplace_count[id] >= get_pcvar_num(g_Settings[SET_MAXOFFERS_MARKET]) )
	{
		if (get_pcvar_num(g_Cvars[CVAR_LOG_LEVEL]) >= 2)
		{
			new name[33]; get_user_name(id, name, 32);
			PrintMessage("Player %s Limit %d | Max %d", name, g_marketplace_count[id], get_pcvar_num(g_Settings[SET_MAXOFFERS_MARKET]))
		}
		return Print(id,"%L", id, "CSRANK_MARKETPLACE_ERROR")
	}

	new data[mpArray];
	
	data[MP_ID] = id;
	data[MP_ITEM] = g_marketplace_item[id];
	data[MP_PRICE] = g_marketplace_price[id];
	data[MP_STATUS] = 1;
	
	data[MP_FOR] = g_marketplace_for[id];

	get_user_name(id, data[MP_NAME], 15)
	
	if (data[MP_FOR] > 0)
		Print(data[MP_FOR], "%L", id, "CSRANK_MARKETPLACE_INFOME",data[MP_NAME])
	
	ArrayPushArray(g_marketplace_offers, data);
	g_marketplace_count[id] ++;
	
	Print(id, "%L", id, "CSRANK_MARKETPLACE_ADD")
	
	if (get_pcvar_num(g_Cvars[CVAR_LOG_LEVEL]) >= 2)
	{
		new name[32]; get_user_name(data[MP_FOR], name, 31)
		PrintMessage("[Market] %s [Offers:%d][Max:%d] create offer | Item [%d] | Cost [%d] | For [%d][%s]", data[MP_NAME], g_marketplace_count[id],get_pcvar_num(g_Settings[SET_MAXOFFERS_MARKET]),data[MP_ITEM], data[MP_PRICE], data[MP_FOR], data[MP_FOR] > 0 ? name : "All")
	}

	return MarketPlaceMenu(id)
}
RemoveAllOffer(id, bool:type = false)
{
	new size = ArraySize(g_marketplace_offers);

	if (size <= 0 || !is_user_connected(id))
		return;

	new data[mpArray];
	
	new count = 0;
	for(new i = 0; i< size; i++)
	{
		ArrayGetArray(g_marketplace_offers, i, data)
		
		if (data[MP_ID] != id || !data[MP_STATUS]) continue;
		
		switch (type)
		{
			case true:
			{
				data[MP_STATUS] = 0;
				ArraySetArray(g_marketplace_offers, i, data);
			}
			default: ArrayDeleteItem(g_marketplace_offers, i);
		}

		g_marketplace_count[id] --;
		count ++;
	}
	
	if (g_marketplace_count[id] < 0 )	g_marketplace_count[id] = 0;

	if (is_user_connected(id)) {
		Print(id, "%L", id, "CSRANK_MARKETPLACE_CLEARALL", count)
		MarketPlaceMenu(id)
	}
	
}
MarketPlaceMenu(id)
{

	new menu, title[128];
	formatex(title, charsmax(title),"[%s \r%s\w] \w%L",MENU_PREFIX,VERSION,\
	 id, "CSRANK_MARKETPLACE_TITLE")
	
	menu = menu_create(title, "HandleMarketPlaceMenu")
	
	formatex(title, charsmax(title),"%L \y[%d]", id, "CSRANK_MARKETPLACE_VIEWALL", CountAllOffers())
	menu_additem(menu, title, "1");
	
	formatex(title, charsmax(title),"%L \y[%d]^n", id, "CSRANK_MARKETPLACE_VIEWME", CountOffersFor(id))
	menu_additem(menu, title, "2");
	
	formatex(title, charsmax(title),"%L", id, "CSRANK_MARKETPLACE_CREATE")
	menu_additem(menu, title, "3");
	
	formatex(title, charsmax(title),"%L^n", id, "CSRANK_MARKETPLACE_CREATEFOR")
	menu_additem(menu, title, "4");
	
	formatex(title, charsmax(title),"%L \y[%d]\r[Max: %d]", id, "CSRANK_MARKETPLACE_VIEWMY", g_marketplace_count[id], get_pcvar_num(g_Settings[SET_MAXOFFERS_MARKET]))
	menu_additem(menu, title, "5");
	
	formatex(title, charsmax(title),"%L", id, "CSRANK_MARKETPLACE_DELETEMY")
	menu_additem(menu, title, "6");

	return menu_display(id, menu, 0)
}
public HandleMarketPlaceMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowMainMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new uid = str_to_num(data);

	switch(uid)
	{
		case 1: ViewAllOffers(id, 0);
		case 2:  ViewAllOffers(id, 1);
		case 3: QuerySkinMenu(id, 1);	
		case 4: QuerySkinMenu(id, 2);	
		case 5: ViewMyOffers(id);
		case 6: RemoveAllOffer(id, true);
	}

	return menu_destroy(menu);
}

ViewMyOffers(id)
{
	new data[mpArray], _data[Items];
	new size = ArraySize(g_marketplace_offers);

	if (size <= 0)
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_CLEAR")

	new count = 0;
	for(new i = 0; i< size; i++)
	{
		ArrayGetArray(g_marketplace_offers, i, data)
		
		if (data[MP_ID] != id || !data[MP_STATUS])	continue;

		ArrayGetArray(g_Items, search_array(data[MP_ITEM]), _data)
		Print(id, "^3#%d ^4%s^1 :: ^1[^4%s^1] :: [^3%d Coin(s)^1]", ++count, data[MP_NAME],  _data[ITEM_NAME], data[MP_PRICE])
	}
	
	MarketPlaceMenu(id)
	return PLUGIN_CONTINUE;
}
ChoosePlayerFor(id)
{
	new menu, title[128];
	formatex(title, charsmax(title),"[%s \r%s\y] \w%L",MENU_PREFIX,VERSION,\
	 id, "CSRANK_INFOMENU_TITLE")
	
	menu = menu_create(title, "HandleChooseForMenu")
	new pid[6], name[32];
	
	static maxplayers;
	if( !maxplayers) maxplayers = get_maxplayers();
	
	for(new player = 1; player<= maxplayers; player++)
	{
		if(!is_user_connected(player) || player == id)
			continue;
	
		num_to_str(player, pid, charsmax(pid))
		get_user_name(player, name, charsmax(name))
		
		menu_additem(menu, name, pid);
	}
	
	menu_display(id, menu, 0)
}
public HandleChooseForMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowMainMenu(id)
		ResetMarket(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new pid = str_to_num(data);
	
	if(!is_user_connected(pid))
		return menu_destroy(menu);
		
	g_marketplace_for[id] = pid;
	
	Print(id, "%L",id, "CSRANK_MARKETPLACE_ENTERCOST")
	client_cmd(id, "messagemode CSR_SetMoney")
	return menu_destroy(menu);
}
ViewAllOffers(id, type = 0)
{
	new menu, title[128];
	formatex(title, charsmax(title),"[%s \r%s\w] \w%L",MENU_PREFIX,VERSION,\
	 id, "CSRANK_MARKETPLACE_TITLE")
	
	menu = menu_create(title, "HandleAllOffers")
	
	new data[mpArray], _data[Items];
	new size = ArraySize(g_marketplace_offers);

	if (size <= 0)
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_CLEAR")

	new index = 0;
	
	switch(type)
	{
		case 1: index = id;
		default: index = 0;
	}

	new text[64], str[12];
	for(new i = 0; i< size; i++)
	{
		ArrayGetArray(g_marketplace_offers, i, data)
		
		if (data[MP_FOR] == index && data[MP_STATUS] == 1)
		{
			ArrayGetArray(g_Items, search_array(data[MP_ITEM]), _data)
		
			formatex(text, charsmax(text), "\w%s-\d[\y%s\d]-[\r%d Coin(s)\d]", data[MP_NAME], _data[ITEM_NAME], data[MP_PRICE])
			formatex(str, charsmax(str), "%d %d", i, data[MP_ITEM])
		
			menu_additem(menu, text, str)
		}
	}

	menu_setprop(menu, MPROP_PERPAGE, 5)
	return menu_display(id, menu, 0)
}
public HandleAllOffers(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		MarketPlaceMenu(id)
		return menu_destroy(menu);
	}
	
	new _data[12], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, _data,11, _dummy, charsmax(_dummy), callback);

	new _uid[5], _check[5];
	parse(_data, _uid, charsmax(_uid), _check, charsmax(_check))
	
	new uid = str_to_num(_uid);
	new check = str_to_num(_check);
	
	if( uid >= ArraySize(g_marketplace_offers) )
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")

	new data[mpArray];
	ArrayGetArray(g_marketplace_offers, uid, data);

	if(data[MP_FOR] != id && data[MP_FOR] != 0)
	{
		new name[33];
		get_user_name(id, name, 32)
		PrintMessage("[CSRErr] Oops, offer for %d, but %s(%d)", data[MP_FOR], name, id)
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")
	}

	if ( g_marketplace_offer_time[id] - get_systime(0) >= 0 )
		return Print(id, "%L", id, g_marketplace_offer_time[id] - get_systime(0));

	g_marketplace_offer_time[id] = get_systime(10);

	new authid[33], ip[22]

	get_user_authid(data[MP_ID], authid, 32)
	get_user_ip(data[MP_ID], ip, 21, 1)
	
	new query[225], Data[3]; Data[0] = id; Data[1] = uid; Data[2] = check;
	formatex(query, charsmax(query), "SELECT * FROM `%s` WHERE (`player_id` = '%s' OR `player_ip` = '%s') LIMIT 1",\
		TABLE, authid, ip)
		
	SQL_ThreadQuery(g_Sqlx, "QueryMarkePlaceEnd", query, Data, 3)
	return menu_destroy(menu);
}
public QueryMarkePlaceEnd(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	new id = Data[0];
	new uid = Data[1];
	new check = Data[2];

	if(SQL_NumResults(Query) <= 0)
		return SQL_FreeHandle(Query);
	
	new name[32];get_user_name(id,name, 31)

	new text[Q_ITEM_SIZE];
	SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_items"), text, charsmax(text))
	
	if (!check_array_value(g_marketplace_offers, uid))
	{
		PrintMessage("[CSRErr] %s Offer has unexisting item [%d] ArraySize [%d]", name, uid, ArraySize(g_marketplace_offers))
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")
	}

	if( uid >= ArraySize(g_marketplace_offers) )
		return Print(id, "%L", id,"CSRANK_MARKETPLACE_ERROR")


	new data[mpArray];
	ArrayGetArray(g_marketplace_offers, uid, data);
	
	if( data[MP_ID] == id )
	{
		PrintMessage("[CSRErr] %s use own offer", name)
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")
	}

	if (data[MP_ITEM] != check || !is_user_connected(data[MP_ID]) || data[MP_STATUS] != 1 ||
		equali( g_player_data[id][PD_IP], g_player_data[data[MP_ID]][PD_IP]))
	{
		if (get_pcvar_num(g_Cvars[CVAR_LOG_LEVEL]) >= 2)
		{
			if(data[MP_ITEM] != check)			PrintMessage("Player %s Invalid data Check [%d][%d]", name,  data[MP_ITEM], check)
			else if(!is_user_connected(data[MP_ID]))	PrintMessage("Player %s offer creator not online", name)
			else if(data[MP_STATUS] != 1)			PrintMessage("Player %s problem with status %d", name, data[MP_STATUS])
			else if (equali( g_player_data[id][PD_IP], g_player_data[data[MP_ID]][PD_IP])) PrintMessage("Player %s has equal ip")
		}
		
		if( data[MP_STATUS] != 1)
			Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR_STATUS") 

		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")
	}
	
	if( g_player_stats[id][COINS] < data[MP_PRICE] )
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_NOCOINS")

	new str_id[10];
	num_to_str(data[MP_ITEM], str_id, charsmax(str_id))
	

	if (containi(text, str_id) == -1 || g_blocked[data[MP_ID]])
	{
		new name2[32];get_user_name(data[MP_ID],name2, 31)
		
		if (containi(text, str_id) == -1)		PrintMessage("Player %s | Player %s don't have item [%s][Need:%s]", name, name2,text,str_id)
		else if (g_blocked[data[MP_ID]])		PrintMessage("Player %s | Player %s have a blocked", name, name2)

		data[MP_STATUS] = 0;
		ArraySetArray(g_marketplace_offers, uid, data)
		return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR")
	}
	
	new bool:ret[2];
	ret[0] = item_add_to_player(g_Sqlx, data[MP_ID], data[MP_ITEM], OperationType:OPER_REMOVE_ITEM, true);

	if ( !ret[0] )	return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR");

	ret[1] = item_add_to_player(g_Sqlx, id, data[MP_ITEM], OperationType:OPER_ADD_ITEM, true);

	if ( !ret[1] )	return Print(id, "%L", id, "CSRANK_MARKETPLACE_ERROR");

	data[MP_STATUS] = 0;
	ArraySetArray(g_marketplace_offers, uid, data)

	AddCoins(data[MP_ID], data[MP_PRICE]);
	RemoveCoins(id, data[MP_PRICE]);

	g_marketplace_count[data[MP_ID]] --;
	
	Print(id, "%L", id, "CSRANK_MARKETPLACE_SUCEESS")
	PrintMessage("[Market]  %s accepted the offer | Creator %s | Item [%d] | Cost [%d] | For [%d]", name, data[MP_NAME], data[MP_ITEM], data[MP_PRICE], data[MP_FOR])
	
	checkSetStatus(id)
	checkSetStatus(data[MP_ID])
	
	return SQL_FreeHandle(Query);
}
public SetMPMoney(id)
{
	static arg[33];
	read_argv(1, arg, charsmax(arg));
	
	if ( !strlen(arg) )
	{
		client_print(id, print_center,"You can't set a transferred points blank! Please type a new value.");
		
		client_cmd(id,"messagemode CSR_SetMoney");
		return PLUGIN_HANDLED;
	}
	else if ( !IsStrFloat(arg) )
	{
		client_print(id, print_center,"You can't use letters in a transferred points! Please type a new value.");
		
		client_cmd(id,"messagemode CSR_SetMoney");
		return PLUGIN_HANDLED;
	}
	new check = str_to_num(arg);
	if(check < 0)
	{
		client_print(id, print_chat,"[CSRank] Value must be > 0");
		
		client_cmd(id,"messagemode CSR_SetMoney");
		return PLUGIN_HANDLED;
	}
	
	g_marketplace_price[id] = check;
	AddToMarketplace(id);
	
	Print(id, "%L", id, "CSRANK_MARKETPLACE_ENTERCOST")
	return PLUGIN_CONTINUE;
}

bool:IsStrFloat(string[])
{
	new len = strlen(string);
	for ( new i = 0; i < len; i++ )
	{
		switch ( string[i] )
		{
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-':	continue;
			default:							return false;
		}
	}
	
	return true;
}

stock CountAllOffers()
{
	new count = 0;
	new data[mpArray];
	new size = ArraySize(g_marketplace_offers);

	for(new i = 0; i< size; i++)
	{
		ArrayGetArray(g_marketplace_offers, i, data)
		
		if ( !data[MP_FOR] )
			count++
	}
	
	return count;
}
stock CountOffersFor( const id )
{
	new count = 0;
	new data[mpArray];
	new size = ArraySize(g_marketplace_offers);

	if (!size) return 0;

	for(new i = 0; i< size; i++)
	{
		ArrayGetArray(g_marketplace_offers, i, data)
		
		if ( data[MP_FOR] == id)
			count++
	}
	
	return count;
}
stock bool:check_array_value(Array: g_arr, value)
{
	if (value < 0 || value >= ArraySize(g_arr))
		return false;
		
	return true;
}
