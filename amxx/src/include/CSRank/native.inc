#if defined _native_included
	#endinput
#endif

#define _native_included

enum
{
	CSR_VALUE_MEDALS = 0,

	CSR_VALUE_EXP,
	CSR_VALUE_COINS,
	CSR_VALUE_CASES,
	CSR_VALUE_KEYS,
	CSR_VALUE_LEVEL
}
public plugin_natives()
{
	register_library("csrank")
	
	register_native("csr_get_value", "_get_exp", 1)
	register_native("csr_set_value", "_set_exp", 1)

	register_native("csr_add_item", "_add_item", 1)
	register_native("csr_remove_item", "_remove_item", 1)
	
	register_native("csr_give_drop", "CmdLevelItem", 1)
}

public _get_exp(id, type)
{
	if(!csr_is_valid_player(id))
		return -1;

	new value = -2;
	switch (type)
	{
		case CSR_VALUE_MEDALS: 	value = g_player_stats[id][MEDAL];
		case CSR_VALUE_EXP: 	value = g_player_stats[id][EXP];
		case CSR_VALUE_COINS: 	value = g_player_stats[id][COINS];
		case CSR_VALUE_CASES: 	value = g_player_stats[id][CASES];
		case CSR_VALUE_KEYS: 	value = g_player_stats[id][KEYS];
		case CSR_VALUE_LEVEL: 	value = g_player_stats[id][LEVEL];
	}

	return value;
}
public _set_exp(id, type, exp)
{
	if(!csr_is_valid_player(id))
		return 0;

	switch (type)
	{
		case CSR_VALUE_MEDALS: 	g_player_stats[id][MEDAL] = exp;
		case CSR_VALUE_EXP: 	{
			g_player_stats[id][EXP] = exp; 
			g_player_stats[id][LEVEL] = LevelUpdate(id, false);
		}
		case CSR_VALUE_COINS: 	g_player_stats[id][COINS] = exp;
		case CSR_VALUE_CASES: 	g_player_stats[id][CASES] = exp;
		case CSR_VALUE_KEYS: 	g_player_stats[id][KEYS] = exp;
		case CSR_VALUE_LEVEL: 	g_player_stats[id][LEVEL] = exp;
	}
	
	return 1;
}

public _add_item(id, const skin_id)
{
	new item_id = skin_id;
	return (csr_is_valid_player(id) && item_add_to_player(g_Sqlx, id, item_id, OperationType:OPER_ADD_ITEM))
}

public _remove_item(id, const skin_id)
{
	new item_id = skin_id;
	return (csr_is_valid_player(id) && item_add_to_player(g_Sqlx, id, item_id, OperationType:OPER_REMOVE_ITEM))
}
