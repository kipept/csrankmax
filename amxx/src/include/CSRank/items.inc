#if defined _items_included
	#endinput
#endif

#define _items_included

#include <amxmodx>

stock const SQL_ITEM_FIELD[] = "player_items";

stock bool:item_add_to_player( Handle:sqlx, id, &item_id, const OperationType:type, bool:priority = false)
{
	new OperationReturn:error;
	new ret = _item_add_to_player(sqlx, id, item_id, OperationType:type, error, priority);

	if ( ret && ret != ACR_RET_NOTNING)
	{
		new error_str[64];
		scase_get_error_string( ret, error, error_str, charsmax(error_str));

		client_print(id, print_chat, "[%s] %s", PLUGIN, error_str)
		return false;
	}

	if (type == OperationType:OPER_REMOVE_ITEM)	ReplaceSkin(id, item_id);

	return true;
}
stock OperationReturn: item_create_menu( id, function[])
{
	return oper_player_items( g_Sqlx, id, _, SQL_ITEM_FIELD, function);
}

stock item_get_info( const item_id, data[Items], bool:search = true)
{
	new index = search ? search_array(item_id) : item_id;

	if (index < 0 )	return ;

	ArrayGetArray(g_Items, index, data)
}
// Add case to player. Use case_id = CASES_GIVE_RANDOM to add a random case to player
stock _item_add_to_player( Handle:sqlx, id, &item_id, const OperationType:type, & OperationReturn:error, bool:priority = false)
{
	if (type != OperationType:OPER_ADD_ITEM && type != OperationType:OPER_REMOVE_ITEM)
		return ACR_RET_ERROR_TYPE;

	if ( !isSkinExist(item_id) )
		return ACR_RET_ERROR_CASE;

	new str[5];
	num_to_str(item_id, str, charsmax(str));

	new OperationReturn:ret;
	switch (type)
	{
		case OPER_ADD_ITEM:ret = oper_player_items(sqlx, id, OPER_ADD_ITEM, SQL_ITEM_FIELD, str, priority );
		case OPER_REMOVE_ITEM:ret = oper_player_items(sqlx, id, OPER_REMOVE_ITEM, SQL_ITEM_FIELD, str, priority );
	}

	if ( ret != OPER_RET_SUCCESS)
	{
		error = ret;
		return ACR_RET_ERROR_OPER;
	}

	return ACR_RET_OK;
}
public CmdLevelItem(id, bool:is_case)
{
	new chanse = get_pcvar_num(g_Settings[SET_INI_ITEM_CHANCE]);
	new key_chance = get_pcvar_num(g_Settings[SET_INI_KEY_CHANCE]);
  
	new name[32];get_user_name(id, name, 31)

	if(get_bit(g_vip, id)) key_chance+= get_pcvar_num(g_Settings[SET_VIP_KEY]);

	if(random_num(0, 100) <= chanse || is_case)
	{
		Print(id,"%L [%d%%]", id, "CSRANK_ADD_ITEM", chanse)
		
		new data[Items], chance;

		new item = CalculateItemChance(id, chance);
		new rnd = calc_items(item);
		
		ArrayGetArray(g_Items, rnd, data)

		if (! item_add_to_player(g_Sqlx, id, data[ITEM_ID], OperationType:OPER_ADD_ITEM, true) )	return -1;

		if ( item > 2)
		{
			new const screenFade[4][3] = {
				{0, 0, 0},
				{0, 0, 0},
				{ 200, 50, 0},
				{ 255, 255, 50}
			}
			log_to_file( "addons/amxmodx/logs/CSRank_take_items.log", "Player %s take %s [ItemClass: %d]", name, data[ITEM_NAME], data[ITEM_CLASS]);
			
			CmdScreenFade(id, 3, screenFade[item-1], 150)
			
			set_dhudmessage(screenFade[item-1][0],screenFade[item-1][1],screenFade[item-1][2],-1.0,0.3, 1 ,0.1, 10.0,0.1,0.1, true)
			show_dhudmessage(0, "%L", LANG_PLAYER, "CSRANK_SKIN_GREAT", name, data[ITEM_NAME], chance)
			
		}
			
		return Print(0, "%L", id, "CSRANK_ADD_SKIN", name, data[ITEM_NAME])
	}

	if(random_num(0,100) <= random_num(0,key_chance))
	{
		g_player_stats[id][KEYS]++;

		if (get_bit(g_vip, id)) g_player_stats[id][KEYS]++;

		return Print(0, "%L", id, "CSRANK_ADD_KEYS", name);
	}

	new OperationReturn:error;
	new case_id = CASES_GIVE_RANDOM;
	new ret = scase_oper_case_to_player(g_Sqlx, g_array_cases, id, case_id, OperationType:OPER_ADD_ITEM, error, true);
	if ( ret && ret != ACR_RET_NOTNING)
	{
		new error_str[64];
		scase_get_error_string( ret, error, error_str, charsmax(error_str));

		return client_print(id, print_chat, "[%s] %s", PLUGIN, error_str)
	}

	if (ret == ACR_RET_NOTNING)
	{
		g_player_stats[id][CASES]++;
		if (get_bit(g_vip, id)) g_player_stats[id][CASES]++;
		return Print(0, "%L", id, "CSRANK_ADD_CASES", name);
	}

	new case_name[64];
	scase_get_case_name(g_array_cases, case_id, case_name, charsmax(case_name));

	return Print(0, "Congratulation!^3 %s ^1got a smart case called^4 '%s'", name, case_name);
}
stock calc_items( const item_class)
{
	new Array: arr = ArrayCreate(1);
			
	new data[Items]
	for(new i = 1; i< ArraySize(g_Items); i++)
	{
		ArrayGetArray(g_Items, i, data)
		
		if(data[ITEM_CLASS] == item_class)
			ArrayPushCell(arr, i);
	}
			
	new rnd = ArrayGetCell(arr,random_num(0, ArraySize(arr) - 1));
	
	ArrayDestroy(arr)
	return rnd;
}

stock get_items_by_level( Array:to, SkinLevel: level )
{
	new data[Items], size = ArraySize(g_Items);

	for( new i = 0; i< size; i++)
	{
		ArrayGetArray(g_Items, i, data);

		if (SkinLevel:data[ITEM_CLASS] == level)
			ArrayPushCell(to, data[ITEM_ID]);
	}

	return 1;
}

// return 0 if item not exists else return item class
stock SkinLevel:get_item_info_by_id( const ItemID )
{
	new find = search_array( ItemID );

	if (find == -1)
		return SL_INVALID;

	new data[Items];
	ArrayGetArray(g_Items, find, data)

	return SkinLevel:data[ITEM_CLASS];
}
stock ReadFile_Skins()
{
	new fileSkin[64]; getDirByType(DIR_CONFIG, fileSkin, charsmax(fileSkin), "Skins.ini")

	new f = fopen(fileSkin, "r")
	
	if(!f)	return;
	
	new filedata[512];
	
	new skin_id[6], item_class[6];
	new data[Items]
	
	while(!feof(f))
	{
		fgets(f, filedata, charsmax(filedata))
		
		if(!filedata[0] || filedata[0] == '#' || filedata[0] == ';'  || filedata[0] != '^"')
			continue;
		
		parse(filedata, skin_id, charsmax(skin_id), data[ITEM_REPLACE], 31, data[ITEM_NAME], 63,\
		data[ITEM_MODEL_V], 63,data[ITEM_MODEL_P], 63,data[ITEM_MODEL_W], 63, item_class, charsmax(item_class))

		data[ITEM_ID] = str_to_num(skin_id);
		data[ITEM_CLASS] = str_to_num(item_class);
	
		ArrayPushArray(g_Items, data)
	}
	
	PrintMessage("[%s] Load %d skins", PLUGIN, ArraySize(g_Items))
}
stock PrecacheSkins()
{
	new size = ArraySize(g_Items);
	new data[Items], bool:ext = false;
	
	new cvar = get_pcvar_num(g_Cvars[CVAR_PRECACHE_ALL]);
	for(new i = 0; i<size; i++)
	{
		ArrayGetArray(g_Items, i, data)
		
		if(file_exists(data[ITEM_MODEL_V]))				precache_model(data[ITEM_MODEL_V])
		else								PrintMessage("[%s] ERROR! Can't find File '%s'",PLUGIN,data[ITEM_MODEL_V])
		
		ext = bool:file_exists(data[ITEM_MODEL_P]);
		
		if( ( ext && data[ITEM_CLASS]>2 ) || (ext && cvar) )		precache_model(data[ITEM_MODEL_P])
		else if ( !ext )						PrintMessage("[%s] ERROR! find File '%s'",PLUGIN,data[ITEM_MODEL_P])
		
		ext = bool:file_exists(data[ITEM_MODEL_W]);
		if( ( ext && data[ITEM_CLASS]>2 ) || (ext && cvar) )		precache_model(data[ITEM_MODEL_W])
		else if ( !ext )						PrintMessage("[%s] ERROR! find File '%s'",PLUGIN,data[ITEM_MODEL_W])
		
	}
}