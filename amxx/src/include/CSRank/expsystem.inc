#if defined _exp_included
	#endinput
#endif

#define _exp_included

#include <amxmodx>
#include <sqlx>

new const MAX_TOP = 15;

new const sEXPGIVE[] = "CSRANK_EXP_GIVE_KILL"
new const sMEDALGIVE[] = "CSRANK_MEDAL_GIVE_KILL"

new const sMESS_LEVELUP[] = "CSRANK_MESS_LEVELUP"

InitExpSystem()
{
	//precache_sound(slevel_up);
	//precache_sound(smedal_up);
	
	g_exp_level = ArrayCreate(1);
}
SetLevelExp()
{
	new str[128];
	get_pcvar_string(g_Cvars[CVAR_LEVEL_EXP], str, charsmax(str))
	
	new exp_parse[6];
	while(strlen(str)>0)
	{
		strbreak(str, exp_parse, charsmax(exp_parse), str, charsmax(str))
		ArrayPushCell(g_exp_level, str_to_num(exp_parse));
	}
	
	PrintMessage("[CSRank] Load %d level(s)", ArraySize(g_exp_level)-1);
}

public ShowTop(id)
{
	new query[200], Data[1]; Data[0] = id;
	formatex(query, charsmax(query), "SELECT * FROM `%s` ORDER BY `player_medal` DESC, `player_exp` DESC",\
	TABLE)
		
	SQL_ThreadQuery(g_Sqlx, "QueryLoadTop", query, Data, 1)
}
public QueryLoadTop(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	new id = Data[0];
	
	if(SQL_NumResults(Query) <= 0)
		return SQL_FreeHandle(Query);
		
	new html_motd[ 2500 ],maxtop;maxtop = 1;
	new  name[32] ,exp, medal;
	new len
	if( !len )
	{
		len = formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "<STYLE>body{background:#808080;color:#000000;font-family:sand-serif}table{width:100%%;font-size:16px}</STYLE><table cellpadding=2 cellspacing=0 border=0>" );
		len += formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "<center>Top Players</center></img>");
		len += formatex ( html_motd [ len ], charsmax ( html_motd ) - len, "<tr align=center bgcolor=%52697B><th width=8%% align=left><font color=white>Rank: <th width=8%% align=left><font color=white>Name: <th width=8%% align=left><font color=white>Medal:EXP:" )
	}
	
	while( SQL_MoreResults(Query))
	{
		
		if(maxtop > MAX_TOP) break;
			
		SQL_ReadResult(Query,SQL_FieldNameToNum(Query,"player_name"),name,31);
		
		exp = SQL_ReadResult(Query,SQL_FieldNameToNum(Query,"player_exp"));
		medal = SQL_ReadResult(Query,SQL_FieldNameToNum(Query,"player_medal"));
	
		len += formatex(html_motd [ len ], charsmax(html_motd)-len, "<tr><td>%i.</td><td>%s</td><td>%i:%d</td></tr>", maxtop, name, medal,exp);
		
		SQL_NextRow(Query);
		maxtop ++
	}
		
	show_motd( id, html_motd, "Top CSRankMax" );	


	return SQL_FreeHandle(Query);
}	
ExpSystem_kill(iKiller, bool:hs = false, weapon[])
{
	
	g_user_kills[iKiller] += _expgive_kill;
	g_player_stats[iKiller][EXP]+=_expgive_kill;

	if(get_bit(g_vip, iKiller))
	{
		//g_user_kills[iKiller] += get_pcvar_num(g_Cvars[CVAR_VIP_BONUS]);
		g_player_stats[iKiller][EXP]+= get_pcvar_num(g_Cvars[CVAR_VIP_BONUS]);
	}
	
	if(hs)	g_player_stats[iKiller][EXP]+= get_pcvar_num(g_Cvars[CVAR_BONUS_HEAD]);	

	if(contain(weapon,"knife") != -1)	g_player_stats[iKiller][EXP]+= get_pcvar_num(g_Cvars[CVAR_BONUS_KNIFE]);		

	g_player_stats[iKiller][EXP] += setCheckExp(iKiller)

	g_player_stats[iKiller][LEVEL] = LevelUpdate(iKiller, false);
	
}
LevelUpdate(id, bool:first = false)
{
	new level = g_player_stats[id][LEVEL];
	new size = ArraySize(g_exp_level) - 1;
	while( g_player_stats[id][EXP] >= ArrayGetCell(g_exp_level, level) && level < size  )
	{
		level++;
		
		if(!first)
		{
			//emit_sound(id, CHAN_STATIC, slevel_up, 1.0, ATTN_NORM, 0, PITCH_NORM);
			client_print_color(id, DontChange, "^1[^3%s^1] %L", PLUGIN, id, sMESS_LEVELUP, level)
			
			CmdScreenFade(id, 1, {0,0,125}, 100)
			
			CmdLevelItem(id, false)
		}
	}
	if(g_player_stats[id][EXP] >= ArrayGetCell(g_exp_level, size))
	{
		g_player_stats[id][MEDAL] ++;
		
		g_player_stats[id][EXP] = 0;
		g_player_stats[id][LEVEL] = 0;
		level = 0;
		
		g_player_stats[id][KEYS] ++;
		
		new name[32];
		get_user_name(id, name, charsmax(name))
		
		//emit_sound(id, CHAN_STATIC, smedal_up, 1.0, ATTN_NORM, 0, PITCH_NORM);
		client_print_color(0, DontChange, "^1[^3%s^1] %L", PLUGIN, id, sMEDALGIVE, name)
		
		CmdScreenFade(id, 5, {200,200,0}, 150)
	}
	
	return level;

}
