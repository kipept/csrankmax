#if defined _smart_cases_included
	#endinput
#endif
 
#define _smart_cases_included

//#define TAG_MODULE
 
#include <amxmodx>

enum OrderType
{
	ORDER_ASC = 0,
	ORDER_DESC
}

// Add a random case;
const CASES_GIVE_RANDOM = -2;

#define MAX_INDEX_LEN 5 // 6 - 1
#define MAX_CASES_LEN 128

stock const SQL_CASE_FIELD[] = "player_smart_cases";
stock const CASE_INI_TAG[] = "[CASE]";

#define DBG

public initSmartCases( Array: cases )
{
	new fileSmartCases[64]; getDirByType(DIR_CONFIG, fileSmartCases, charsmax(fileSmartCases), "SmartCases.ini")
	if ( !file_exists(fileSmartCases) )
		return PrintMessage("Smart Cases ini file not found, check [%s]", fileSmartCases)
	
	parse_cases_from_file( fileSmartCases, cases);
	return 2;
}

stock bool:scase_oper_player( Handle:sqlx, id, &item_id, const OperationType:type, bool:priority = false)
{
	new OperationReturn:error;
	new ret = scase_oper_case_to_player(sqlx, g_array_cases, id, item_id, OperationType:type, error, priority);

	if ( ret && ret != ACR_RET_NOTNING)
	{
		new error_str[64];
		scase_get_error_string( ret, error, error_str, charsmax(error_str));

		client_print(id, print_chat, "[%s] %s", PLUGIN, error_str)
		return false;
	}

	return true;
}

stock scase_give_item_from_case( Array:cases, id, const true_case_id)
{
	// Получаем case_id из Array:cases
	new data[CaseData];
	ArrayGetArray(cases, true_case_id, data);

	// Создаем массив под предметы каждого класса
	new Array: items[4], size = ArraySize(data[CD_ITEMS]);
	new _data[Items]

	for(new i;i<4;i++)	items[i] = ArrayCreate();

	
	new n[32]; get_user_name(id, n, 31)

	new dbg_item[128];
	for(new i; i<size;i++)	formatex(dbg_item, 127, "%s %d", dbg_item,ArrayGetCell(data[CD_ITEMS], i));

	log_to_file("addons/amxmodx/logs/CSRank_dbg_smart.log", "[TakeCases] %s | case_size %d | items [%s]", n, size, dbg_item);

	new find = -1;
	//Проходим по всем предметам в кейсе, и распределяем их по массивам в зависимости от их класса
	for(new i; i<size; i++)
	{
		find = search_array(ArrayGetCell(data[CD_ITEMS], i));

		if (find == -1)	continue;

		ArrayGetArray(g_Items, find, _data);
		ArrayPushCell(items[_data[ITEM_CLASS] -1], find);
	}

	new rnd[4];
	for(new i = 0; i<4; i++)
	{
		size = ArraySize(items[i]);
		//server_print("Size: #%d", size);
		rnd[i] = size > 0 ? ArrayGetCell(items[i], random_num(0, size-1)) : -1;
	}	

	log_to_file("addons/amxmodx/logs/CSRank_dbg_smart.log", "[TakeCases] Rnd[0++]: %d | %d | %d |%d", rnd[0], rnd[1], rnd[2], rnd[3]);

	new item = -3;

	new arr_ch[4];
	arr_ch[0] = CalculateSmartItem(id,4);
	arr_ch[1] = CalculateSmartItem(id,3);
	arr_ch[2] = CalculateSmartItem(id,2);
	arr_ch[3] = CalculateSmartItem(id,1);

	new chance = random_num(0,100);
	for(new i = 3; i>=0; i--)
	{
		if(rnd[i] < 0  || chance > arr_ch[i] )	continue;

		item = rnd[i];
		break;
	}

	if ( item < 0 )	
	{
		for(new i; i<4;i++)	
		{
			if (rnd[i] < 0) continue;

			item = rnd[i];
			break;
		}  
	}

	new item_id = item;

	if( item_id == -1)	return;

	ArrayGetArray(g_Items, item_id, _data)

	if (! item_add_to_player(g_Sqlx, id, _data[ITEM_ID], OperationType:OPER_ADD_ITEM, true) )	return;

	new name[32]; get_user_name(id, name, charsmax(name));
	log_to_file("addons/amxmodx/logs/CSRank_open_cases.log", "[Smart] %s | Great:%d | Item: %s", name, chance, data[ITEM_NAME])

	if( _data[ITEM_CLASS] > 2 )
	{
		log_to_file( "addons/amxmodx/logs/CSRank_take_items.log", "[Smart]Player %s take %s", name, _data[ITEM_NAME]);
			
		set_dhudmessage(0,100,200,-1.0,0.3, 1 ,0.1, 10.0,0.1,0.1, true)
		show_dhudmessage(0, "%L", LANG_PLAYER, "CSRANK_SKIN_GREAT", name, _data[ITEM_NAME], chance)
	}

	client_print_color(0, DontChange, "%s %L ^4<%s>",CHAT_PREFIX, id, "CSRANK_ADD_SKIN", name, _data[ITEM_NAME], data[CD_NAME])
}

// Add case to player. Use case_id = CASES_GIVE_RANDOM to add a random case to player
stock scase_oper_case_to_player( Handle:sqlx, Array:cases, id, &case_id, const OperationType:type, & OperationReturn:error, bool:prior = false)
{
	if (type != OperationType:OPER_ADD_ITEM && type != OperationType:OPER_REMOVE_ITEM)
		return ACR_RET_ERROR_TYPE;

#if defined DEBUG
	server_print("[SCaseOperCase] ID %d | Case_id %d", id, case_id);
#endif

	if ( case_id == CASES_GIVE_RANDOM && type == OperationType:OPER_ADD_ITEM )
	{
		new data[CaseData], size = ArraySize(cases);

		new rnd = random_num(1, 100);
		for(new i; i< size; i++)
		{
			ArrayGetArray(cases, i, data);

			if ( !data[CD_CHANCE])	continue;

			if (rnd < random(data[CD_CHANCE]) )
			{
				case_id = data[CD_INDEX];
				break;
			}
		}

		if ( case_id == CASES_GIVE_RANDOM )	return ACR_RET_NOTNING;

		if (get_pcvar_num(g_Cvars[CVAR_LOG_LEVEL]) > 2) {
		new name[32];get_user_name(id, name, charsmax(name))
		new caseName[64]; scase_get_case_name(cases, case_id, caseName, 63)
		log_to_file("addons/amxmodx/logs/CSRank_SmartCase.log", " Player %s take case %s [Random:%d][CaseChance:%d]", name, data[CD_NAME], rnd, data[CD_CHANCE])
		}

#if defined DEBUG
		server_print("[SCaseOperCase] GetRandomCase: %d",case_id);
#endif
	}

	if ( scase_find_case_by_id(cases, case_id) == -1)
		return ACR_RET_ERROR_CASE;

	new str[5];
	num_to_str(case_id, str, charsmax(str));

	new OperationReturn:ret;
	switch (type)
	{
		case OPER_ADD_ITEM:ret = oper_player_items(sqlx, id, OPER_ADD_ITEM, SQL_CASE_FIELD, str, prior );
		case OPER_REMOVE_ITEM:ret = oper_player_items(sqlx, id, OPER_REMOVE_ITEM, SQL_CASE_FIELD, str, prior );
	}

	if ( ret != OPER_RET_SUCCESS)
	{
		error = ret;
		return ACR_RET_ERROR_OPER;
	}

	return ACR_RET_OK;
}

stock scase_get_error_string( ret, const OperationReturn: error, str[], len)
{
	if ( ret == ACR_RET_OK)	return;

	switch (ret)
	{
		case ACR_RET_ERROR_TYPE: formatex(str, len, "Operation type error for smart cases. Avaliable only add or remove item");
		case ACR_RET_ERROR_CASE: formatex(str, len, "Smart case with this id not exists");
		case ACR_RET_ERROR_OPER: oper_get_error_string( error, str, len);
	}

}
stock OperationReturn: scase_create_case_menu( id, function[])
{
	return oper_player_items( g_Sqlx, id, _, SQL_CASE_FIELD, function);
}

stock scase_get_case_name( Array:cases, case_id, str[], len)
{
	new find = scase_find_case_by_id(cases, case_id);
	if ( find == -1)
		return ACR_RET_ERROR_CASE;

	new data[CaseData];
	ArrayGetArray(cases, find, data)

	copy( str, len, data[CD_NAME]);
	return ACR_RET_OK;
}
stock scase_find_case_by_id( Array: cases, case_id )
{
	new data[CaseData];
	new size = ArraySize(cases);

	for( new i  = 0; i<size; i++)
	{
		ArrayGetArray(cases, i, data);

		if ( data[CD_INDEX] == case_id )
			return i;
	}

	return -1;
}


// Stock **********************************************
stock parse_cases_from_file( const file[], Array: cases)
{
	new f = fopen( file, "r");

	if ( !f )	return PrintMessage("Can't open file for read [%s]", file);

	new filedata[256], bool:index;

	new data[CaseData], cindex[6], cchance[6], ckeys[6], cshop[6], csale[6], ccolor[6];

	while (!feof(f))
	{
		fgets(f, filedata, charsmax(filedata));
		
		trim(filedata);
		if(!filedata[0] || filedata[0] == '#')
			continue;

		if(equali(filedata, CASE_INI_TAG, strlen(CASE_INI_TAG)))
		{
			index = !index;

			if ( !index )
			{

				if ( ArraySize(data[CD_ITEMS]) <= 0)	continue;

				case_items_to_order(data[CD_ITEMS], .order = ORDER_DESC)

				//log_amx("Case '%s' ID '%d'", data[CD_NAME], data[CD_INDEX])
				//PrintArrayCell(data[CD_ITEMS]);

				ArrayPushArray(cases, data);
				continue;
			}
			replace(filedata, charsmax(filedata), CASE_INI_TAG, "")
			trim(filedata)
			
			parse(filedata, cindex, MAX_INDEX_LEN, data[CD_NAME], charsmax(data[CD_NAME]), cchance, MAX_INDEX_LEN, ckeys, MAX_INDEX_LEN,
			cshop, MAX_INDEX_LEN, csale, MAX_INDEX_LEN, ccolor, MAX_INDEX_LEN)
			
			data[CD_INDEX] = str_to_num(cindex);
			data[CD_CHANCE] = str_to_num(cchance);
			data[CD_OPEN_KEYS] = str_to_num(ckeys);
			data[CD_SHOP] = str_to_num(cshop);
			data[CD_SALE] = str_to_num(csale);

			// Cases Colors
			if (strlen(ccolor) <= 0)	data[CD_COLOR] = "\w";
			else
			{
				switch ( ccolor[0] )
				{
					case 'W': data[CD_COLOR] = "\w";
					case 'D': data[CD_COLOR] = "\d";
					case 'Y': data[CD_COLOR] = "\y";
					case 'R': data[CD_COLOR] = "\r";

					default: data[CD_COLOR] = "\w";
				}
			}
			
			data[CD_ITEMS] = _:ArrayCreate(1);
			continue;
		}

		if ( !isdigit(filedata[0]) && !add_items_to_case_by_color(data[CD_ITEMS], filedata[0]) ) 	return PrintMessage(" Unkown options at [%s]", filedata);
		else if (isdigit(filedata[0]))	ArrayPushCell(data[CD_ITEMS], str_to_num(filedata));
	}


	SortCases( cases );

	PrintMessage("Load %d Case(s)", ArraySize(cases));

	return fclose(f);
}

stock PrintArrayCell( Array:cell)
{
	for (new i = 0; i< ArraySize(cell); i++)
		log_amx("[%d] %d", i, ArrayGetCell(cell, i))
}

stock SortCases( Array: cases, OrderType: order = ORDER_ASC )
{
	new Data[1]; Data[0] = _:order;
	ArraySort(cases, "SortCasesbyChance", Data, 1)
}
stock case_items_to_order( Array: items, OrderType: order = ORDER_ASC)
{
	new Data[1]; Data[0] = _:order;
	ArraySort(items, "SortItems", Data, 1)
}

public SortCasesbyChance(Array: array, item1, item2, const data[], data_size)
{
	new OrderType:order = OrderType: data[0];

	new item_value[2][CaseData];

	ArrayGetArray(array, item1, item_value[0]);
	ArrayGetArray(array, item1, item_value[1]);

	new chance1 = item_value[0][CD_CHANCE];
	new chance2 = item_value[1][CD_CHANCE];

	switch (order)
	{
		case ORDER_ASC:
		{
			if(chance1 > chance2)	return 1;
			else					return -1;
		}
		case ORDER_DESC:
		{
			if(chance1 > chance2)	return -1;
			else					return 1;
		}
	}

	return 0;
}

public SortItems(Array: array, item1, item2, const data[], data_size)
{
	new OrderType:order = OrderType: data[0];

	new item_value1 = ArrayGetCell(array, item1);
	new item_value2 = ArrayGetCell(array, item2);

	new SkinLevel:skin1 = get_item_info_by_id( item_value1 );
	new SkinLevel:skin2 = get_item_info_by_id( item_value2 );

	switch (order)
	{
		case ORDER_ASC:
		{
			if(skin1 > skin2)	return 1;
			else				return -1;
		}
		case ORDER_DESC:
		{
			if(skin1 > skin2)	return -1;
			else				return 1;
		}
	}

	return 0;
}
stock add_items_to_case_by_color( Array:to, const color )
{
	switch (color)
	{
		case 'g': get_items_by_level(to, SL_GREY);
		case 'w': get_items_by_level(to, SL_WHITE);
		case 'r': get_items_by_level(to, SL_RED);
		case 'y': get_items_by_level(to, SL_YELLOW);
		default: return 0;
	}

	return 1
}

#if defined TAG_MODULE
	#undef PrintMessage
#endif