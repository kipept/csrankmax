#if defined _stock
	#endinput 
#endif

#define _stock

#include <amxmodx>
#include <amxmisc>

stock cvar_flags (const cvar)
{
	new _str[16];
	get_pcvar_string(cvar, _str, charsmax(_str))
	
	return read_flags(_str);
}

stock get_item_price( const item_class)
{
	switch (item_class)
	{
		case 4: return get_pcvar_num(g_Settings[SET_LEG_PRICE])
		case 3: return get_pcvar_num(g_Settings[SET_SPEC_PRICE])
		case 2: return get_pcvar_num(g_Settings[SET_RARE_PRICE])
		case 1: return get_pcvar_num(g_Settings[SET_NORMAL_PRICE])
	}
	
	return -2;
}

stock SetCvars()
{
	#define CONFIG "CSRank.cfg"
	
	g_Cvars[CVAR_MYSQL_HOST] =			register_cvar("csrank_mysql_host", "localhost")
	g_Cvars[CVAR_MYSQL_USER] = 			register_cvar("csrank_mysql_user", "root")
	g_Cvars[CVAR_MYSQL_PASS] = 			register_cvar("csrank_mysql_pass", "")
	g_Cvars[CVAR_MYSQL_DB] = 			register_cvar("csrank_mysql_db", "")
	g_Cvars[CVAR_MYSQL_TABLE] = 			register_cvar("csrank_mysql_table", "CSRank")
	
	g_Cvars[CVAR_INFORMER] = 			register_cvar("csrank_informer", "1")
	g_Cvars[CVAR_INFORMER_X] = 			register_cvar("csrank_informer_x", "0.01")
	g_Cvars[CVAR_INFORMER_Y] = 			register_cvar("csrank_informer_y", "0.2")
	g_Cvars[CVAR_INFORMER_TEXT] = 			register_cvar("csrank_informer_text", "%name%%n%Level: %level%/%max_level%%n%Exp: %exp%/%exp_next%%n%Tag: %tag%")

	g_Cvars[CVAR_MAIN_FLAG] = 			register_cvar("csrank_main_flag", "l")
	g_Cvars[CVAR_LEVEL_EXP] = 			register_cvar("csrank_level_exp", "100 200 400 700 1000 1500 2100 2800 3600 4500")
	
	g_Cvars[CVAR_BONUS_HEAD] = 			register_cvar("csrank_bonus_exp_head", "1")
	g_Cvars[CVAR_BONUS_KNIFE] = 			register_cvar("csrank_bonus_exp_knife", "2")
	
	g_Cvars[CVAR_VIP_FLAG] = 			register_cvar("csrank_vip_flag", "");
	g_Cvars[CVAR_VIP_BONUS] = 			register_cvar("csrank_vip_bonus_exp", "1");
	
	g_Settings[SET_VIP_ITEM] = 			register_cvar("csrank_vip_bonus_item", "0");
	g_Settings[SET_VIP_KEY] = 			register_cvar("csrank_vip_bonus_key", "0");
	
	g_Settings[SET_INI_ITEM_CHANCE] = 		register_cvar("csrank_init_chance_item", "40");
	g_Settings[SET_INI_KEY_CHANCE] = 		register_cvar("csrank_init_chance_key", "30");
	
	g_Settings[SET_INI_ITEMTIME] = 			register_cvar("csrank_ini_chance_timeitem", "5");
	g_Settings[SET_CHECK_INT] = 			register_cvar("csrank_check_item_seconds", "150.0");
	g_Settings[SET_MIN_TIME_ITEM] = 			register_cvar("csrank_min_time_take_itemtime", "600");
	
	g_Settings[SET_CASE_PRICE] = 			register_cvar("csrank_cases_price", "5");
	g_Settings[SET_KEY_PRICE] = 			register_cvar("csrank_key_price", "15");
	
	g_Settings[SET_SALE_VALUE] = 			register_cvar("csrank_sale_value", "60");
	
	g_Settings[SET_LEG_PRICE] = 			register_cvar("csrank_legendary_price", "100");
	g_Settings[SET_SPEC_PRICE] = 			register_cvar("csrank_special_price", "50");
	g_Settings[SET_RARE_PRICE] = 			register_cvar("csrank_rare_price", "10");
	g_Settings[SET_NORMAL_PRICE] = 			register_cvar("csrank_normal_price", "2");
	
	g_Settings[SET_MAXOFFERS_MARKET] = 		register_cvar("csrank_marketplace_maxoffers", "1");
	g_Settings[SET_CLEAR_LOGS] = 			register_cvar("csrank_clear_logs", "7");

	g_Cvars[CVAR_PRECACHE_ALL] =			register_cvar("csrank_precache_all", "0")
	g_Cvars[CVAR_LOG_LEVEL] =				register_cvar("csrank_log_level", "3");

	g_Cvars[CVAR_DELETE_DAY] =				register_cvar("csrank_delete_players_day", "30")

	g_Cvars[CVAR_WISH_ADD] =				register_cvar("csrank_wish_time_add", "1")
	g_Cvars[CVAR_WISH_COST] =				register_cvar("csrank_wish_cost", "25")

	new execPath[64]
	getDirByType(DirData:DIR_CONFIG, execPath, charsmax(execPath), CONFIG)

	server_cmd("exec %s",execPath)
	server_exec()
	
	#undef CONFIG
}
stock search_array(uint)
{
	new data[Items]
	for(new i = 0; i<ArraySize(g_Items); i++)
	{
		ArrayGetArray(g_Items, i, data);

		if(data[ITEM_ID] == uint)
			return i;
	}
	
	return -1;
}

stock AddCoins(id, coins)
{
	g_player_stats[id][COINS] +=coins;
	g_player_stats[id][TOTAL_COINS] +=coins;
}
stock RemoveCoins(id, coins)
{
	g_player_stats[id][COINS] -=coins;
}
stock get_next_level(exp)
{
	new iExp = 0;
	new size = ArraySize(g_exp_level)
	for(new iLevel = 0; iLevel < size; iLevel++) {
		iExp = ArrayGetCell(g_exp_level, iLevel);

		if ( exp < iExp) break;
	}
	
	return iExp;
}

stock SQL_Error(Handle:query, const error[], errornum, failstate)
{
	new qstring[512]
	SQL_GetQueryString(query, qstring, 511)
	
	if(failstate == TQUERY_CONNECT_FAILED) 
	{
		PrintMessage("[SQLX] Error connected to database")
	} 
	else if (failstate == TQUERY_QUERY_FAILED) 
	{
		PrintMessage("[SQLX] Failed")
	}
	
	PrintMessage("[SQLX] Error '%s' with '%s'", error, errornum)
	PrintMessage("[SQLX] %s", qstring)

	return SQL_FreeHandle(query)
}
stock ResetPlayer(id)
{
	for(new i; i < sizeof(g_player_stats[]); i++)
	{
		g_player_stats[id][i] = _:0;
	}
	
	g_blocked[id] = false;
	g_user_kills[id] = 0;
	g_take_item[id] = false;
	g_block_player[id] = false;
	ResetMarket(id);
}
ResetMarket(id)
{
	g_marketplace_count[id] 		= 0;
	g_marketplace_item[id] 			= 0;
	g_marketplace_price[id] 		= 0;
	g_marketplace_for[id] 			= 0;
	g_marketplace_time[id] 			= 0;
	g_marketplace_offer_time[id] 	= 0;
}

stock MakeStringSQLSafe(const input[], output[], len)
{
	copy(output, len, input);
	replace_all(output, len, "'", "*");
	replace_all(output, len, "^"", "*"); // ^"
	replace_all(output, len, "`", "*");
}
stock CmdScreenFade(id, Timer, Colors[3], Alpha) 
{	
	static msg_ScreenFade
	
	if(!msg_ScreenFade)
		msg_ScreenFade = get_user_msgid("ScreenFade");
		
	message_begin(MSG_ONE_UNRELIABLE, msg_ScreenFade, _, id);
	write_short((1<<12) * 2* Timer);
	write_short(1<<12);
	write_short(0);
	write_byte(Colors[0]);
	write_byte(Colors[1]);
	write_byte(Colors[2]);
	write_byte(Alpha);
	message_end();
}
get_player_fastcmd( arg[], arglen )
{
	new is_ip = contain(arg, ".");
	new is_uid = contain(arg, "#");
	
	new player = -1
	
	if(is_ip >= 0)
		player = find_player("d", arg)
	else if (is_uid >= 0)
	{
		replace(arg, arglen,"#","")
		player = find_player("k", str_to_num(arg))
	}
		
	return player;
}	
public CmdResetPlayer(id)
{
	new target[21];
	read_argv(1, target, sizeof(target)-1) 

	new player = get_player_fastcmd(target, sizeof(target) - 1);

	if( player == -1)	return server_print("Player with '%s' not found [IP/#userid]", target)

	new query[128], Data[21]; copy(Data, sizeof(Data) -1, target);
	formatex(query, charsmax(query), "DELETE FROM `%s` WHERE `player_id`='%s' or `player_ip`='%s'", g_player_data[id][PD_AUTHID], g_player_data[id][PD_IP])

	return SQL_ThreadQuery(g_Sqlx, "QueryClearPlayer", query, Data, sizeof(Data))
}
public QueryClearPlayer(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
		return SQL_Error(Query, Error, Errcode, FailState);

	new target[21];
	copy(target, sizeof(target) - 1, Data)

	PrintMessage("[RESET Player] player '%s' reseted", target);

	return SQL_FreeHandle(Query);
}
public CSRAddRank(id)
{
	if( !has_access(id, g_Cvars[CVAR_MAIN_FLAG]))
		return console_print(id, "[%s] Access denied", PLUGIN)
		
	new arg[35];
	read_argv(1, arg, sizeof(arg) - 1);
	
	new target = cmd_target(id, arg, CMDTARGET_NO_BOTS);
	if( !target ) return PLUGIN_HANDLED;
	
	read_argv(2, arg, sizeof(arg) - 1);
	new POINT = str_to_num(arg);
	
	if( POINT <= 0 )
	{
		console_print(id, "Not enough points!");
		return PLUGIN_HANDLED;
	}
	
	g_player_stats[target][EXP] +=POINT;
	
	g_player_stats[target][LEVEL] = LevelUpdate(id, false);
	
	new name[2][32];
	get_user_name(id, name[0], sizeof(name[]) - 1);
	get_user_name(target, name[1], sizeof(name[]) - 1);
	
	client_print(id, print_console, "[%s] You gave %s %i Exp!",PLUGIN, name[1], POINT);
	PrintMessage("%s gave %i Exp to %s", name[0], POINT, name[1]);
	
	return PLUGIN_HANDLED;
}
public CSRAddSkin(id)
{
	if(!has_access(id, g_Cvars[CVAR_MAIN_FLAG]))
		return console_print(id, "[%s] Access denied", PLUGIN)
		
	new arg[35];
	read_argv(1, arg, sizeof(arg) - 1);
	
	new target = cmd_target(id, arg, CMDTARGET_NO_BOTS);
	if( !target ) return PLUGIN_HANDLED;
	
	read_argv(2, arg, sizeof(arg) - 1);
	new POINT = str_to_num(arg);
	
	if( POINT <= 0 )
	{
		console_print(id, "Not enough points!");
		return PLUGIN_HANDLED;
	}
	
	if (! item_add_to_player(g_Sqlx, target, POINT, OperationType:OPER_ADD_ITEM) )	return -1;
	
	new name[2][32];
	get_user_name(id, name[0], sizeof(name[]) - 1);
	get_user_name(target, name[1], sizeof(name[]) - 1);
	
	client_print(id, print_console, "[%s] You gave %s skin #%d!",PLUGIN, name[1], POINT);	
	PrintMessage("%s gave skin #%d to %s", name[0],  POINT, name[1]);
	
	return PLUGIN_HANDLED;
}
public CSRAddCoins(id)
{
	if(!has_access(id, g_Cvars[CVAR_MAIN_FLAG]))
		return console_print(id, "[%s] Access denied", PLUGIN)
		
	new arg[35];
	read_argv(1, arg, sizeof(arg) - 1);
	
	new target = cmd_target(id, arg, CMDTARGET_NO_BOTS);
	if( !target ) return PLUGIN_HANDLED;
	
	read_argv(2, arg, sizeof(arg) - 1);
	
	new type = str_to_num(arg);
	
	if(type > 3 || type < 0)
	{
		console_print(id, "Not enough points!");
		return PLUGIN_HANDLED;
	}

	read_argv(3, arg, sizeof(arg) - 1);

	new POINT = str_to_num(arg);
	
	if( POINT <= 0 )
	{
		console_print(id, "Not enough points!");
		return PLUGIN_HANDLED;
	}
	
	switch(type)
	{
		case 0: AddCoins(target, POINT)
		case 1: g_player_stats[target][CASES]+=POINT;
		case 2: g_player_stats[target][KEYS] +=POINT;
		case 3: g_player_stats[target][WISHES] +=POINT
	}
	
	new name[2][32];
	get_user_name(id, name[0], sizeof(name[]) - 1);
	get_user_name(target, name[1], sizeof(name[]) - 1);
	
	client_print(id, print_console, "[%s] You gave %s %d: %d coins!",PLUGIN, name[1], type, POINT);
	
	static const szType[][] = 
	{
		"Coins",
		"Cases",
		"Keys"
	}

	PrintMessage("%s gave %d: %d to %s", name[0], szType[type], POINT, name[1]);
	
	return PLUGIN_HANDLED;
}
public CSRAddSCase(id)
{
	if(!has_access(id, g_Cvars[CVAR_MAIN_FLAG]))
		return console_print(id, "[%s] Access denied", PLUGIN)
		
	new arg[35];
	read_argv(1, arg, sizeof(arg) - 1);
	
	new target = cmd_target(id, arg, CMDTARGET_NO_BOTS);
	if( !target ) return PLUGIN_HANDLED;
	
	read_argv(2, arg, sizeof(arg) - 1);
	new case_id = str_to_num(arg);
	
	read_argv(3, arg, sizeof(arg) - 1);

	new type = 0;
	if ( strlen(arg) > 0)	type = 1;

	switch (type)
	{
		case 0: if ( !scase_oper_player( g_Sqlx, target, case_id, OperationType:OPER_ADD_ITEM) )	return -1;
		case 1: if ( !scase_oper_player( g_Sqlx, target, case_id, OperationType:OPER_REMOVE_ITEM) )	return -1;
	}
	
	
	new tname[32]; get_user_name(target, tname, charsmax(tname));
	
	new case_name[64];
	scase_get_case_name( g_array_cases, case_id, case_name, charsmax(case_name));

	console_print(id, "[%s] You %s case '%s' to player %s", PLUGIN, !type ? "give" : "remove", case_name, tname)
	return PLUGIN_HANDLED;
}
stock PrintMessage(const szMessage[], any:...)
{
	new szMsg[196];
	vformat(szMsg, charsmax(szMsg), szMessage, 2);
	
	new LogDat[16]
	get_time("%Y_%m_%d", LogDat, 15);

	new LogFile[64];
	getDirByType(DirData:DIR_LOG, LogFile, charsmax(LogFile), "Log%s.log", LogDat)
	log_to_file(LogFile,"[%s] %s",PLUGIN,szMsg)
	
	return -2;
}


stock ClearLogs()
{
	new Array: Logs;
	Logs = ArrayCreate(128);
	
	new szFile[64];
	new LogDir[64];
	getDirByType(DirData:DIR_LOG, LogDir, charsmax(LogDir), "")
	
	new dir = open_dir(LogDir, szFile, charsmax(szFile))
	
	new temp[64], log_time; new del_time = get_systime(0) - 60*60*24*get_pcvar_num(g_Settings[SET_CLEAR_LOGS])
	if(dir)
	{
		do
		{
			if(!is_log_file(szFile, strlen(szFile)))
				continue;
			
			copy(temp, charsmax(temp), szFile)

			
			replace(temp, charsmax(temp), "Log_", "")
			
			
			log_time = parse_time(temp, "%Y_%m_%d")
			
			if(log_time - del_time <= 0)
				ArrayPushString(Logs,szFile)
		}
		while(next_file(dir, szFile, charsmax(szFile)))
		
		close_dir(dir)
	}
	
	new size = ArraySize(Logs)
	for(new i; i<size; i++)
	{
		ArrayGetString(Logs, i, temp, charsmax(temp))
		
		formatex(szFile, charsmax(szFile), "%s%s",LogDir,temp)
		delete_file(szFile)
	}
	
	ArrayDestroy(Logs)
}
stock bool:is_log_file(CurrNAME[], len  )
{
	static S_TRY[] = ".log"
	
	if ( ( len >= 4 ) && ( CurrNAME[ len - 1 ] == S_TRY[ 3 ] ) && 
	( CurrNAME[ len - 2 ] == S_TRY[ 2 ] ) && ( CurrNAME[ len - 3 ] == S_TRY[ 1 ] ) && 
	( CurrNAME[ len - 4 ] == S_TRY[ 0 ] ) ) 
	return true;
	
	return false;
}