#if defined _operataions_included
	#endinput
#endif

#define _operataions_included

#include <amxmodx>
#include <sqlx>

const Q_MAX_LEN = 512;

new g_block_player[33];

enum OperationsType
{
	OPER_GET_ITEMS = 0,
	OPER_ADD_ITEM,
	OPER_REMOVE_ITEM
}

enum OperationReturn
{
	OPER_RET_ERROR = 0,
	OPER_RET_ERROR_MULTI,
	OPER_PLAYER_NOT_LOAD,
	OPER_RET_SUCCESS
}

enum _:OperArray
{
	Handle:oa_sqlx = 0,
	oa_player_id,
	OperationsType:oa_oper_type,
	oa_table_field[32],
	oa_function[32],
	bool:oa_priority
}

stock OperationReturn:oper_player_items(Handle:sqlx, pid, const OperationsType:type = OPER_GET_ITEMS, const fieldName[], function[], bool:priority = false)
{
	if ( !is_user_connected(pid) || sqlx == Empty_Handle )
		return OPER_RET_ERROR;

	if ( !g_player_stats[pid][LOAD] )
		return OPER_PLAYER_NOT_LOAD;

	if ( g_block_player[pid] )
	{
		if (priority)
		{
			new Data[OperArray];

			Data[oa_sqlx] = _:sqlx;
			Data[oa_player_id] = pid;
			Data[oa_oper_type] = _:type;
			copy(Data[oa_table_field], charsmax(Data[oa_function]), fieldName);
			copy(Data[oa_function], charsmax(Data[oa_function]), function);

			Data[oa_priority] = _:priority;
			set_task(random_float(1.0,3.0), "oper_priority_func", random(30000), Data, sizeof Data);
			return OPER_RET_SUCCESS;
		}
		return OPER_RET_ERROR_MULTI;
	}

	if ( type != OPER_GET_ITEMS)
	{
		// Block user
		g_block_player[pid] = true;
	}
	
	new query[Q_ITEM_SIZE], Data[OperArray];

	Data[oa_sqlx] = _:sqlx;
	Data[oa_player_id] = pid;
	Data[oa_oper_type] = _:type;
	copy(Data[oa_table_field], charsmax(Data[oa_function]), fieldName);
	copy(Data[oa_function], charsmax(Data[oa_function]), function);

	new ip[26], authid[26];
	get_user_ip(pid, ip, charsmax(ip), .without_port = 1);
	get_user_authid(pid, authid, charsmax(authid));

	formatex(query, charsmax(query), "SELECT * FROM `%s` WHERE (`player_id` = '%s' OR `player_ip` = '%s') LIMIT 1",\
	TABLE, authid, ip)
	
	SQL_ThreadQuery(sqlx, "QueryOperPlayerItems", query, Data, sizeof Data)
	return OPER_RET_SUCCESS;
}

stock Array:oper_parse_items_from_text ( strText[], len )
{
	new Array:array = ArrayCreate();

	trim(strText);

	new szPart[6];
	while ( strlen(strText) > 0)
	{
		strbreak(strText, szPart, charsmax(szPart), strText, len)
		ArrayPushCell(array, str_to_num(szPart));
	}

	return array;
}

public oper_priority_func(data[OperArray], taskid)	oper_player_items(data[oa_sqlx], data[oa_player_id], data[oa_oper_type], data[oa_table_field], data[oa_function], data[oa_priority]);

public QueryOperPlayerItems(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		if ( Data[oa_oper_type] != OPER_GET_ITEMS ) g_block_player[Data[oa_player_id]] = false;
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	if(SQL_NumResults(Query) <= 0)
	{
		if ( Data[oa_oper_type] != OPER_GET_ITEMS ) g_block_player[Data[oa_player_id]] = false;
		return SQL_FreeHandle(Query);
	}
		
	new text[Q_ITEM_SIZE];
	SQL_ReadResult(Query, SQL_FieldNameToNum(Query, Data[oa_table_field]), text, charsmax(text));

	switch ( Data[oa_oper_type] )
	{
		case OPER_GET_ITEMS:
		{
			new func = get_func_id( Data[oa_function], .pluginId = -1);

			callfunc_begin_i(func, .plugin = -1);
			callfunc_push_int( Data[oa_player_id] );
			callfunc_push_str( text, false );
			callfunc_push_int(sizeof text);
			callfunc_end();

			return SQL_FreeHandle(Query);
		}
		case OPER_ADD_ITEM, OPER_REMOVE_ITEM:
		{

			if (strlen(text) >= Q_ITEM_SIZE - 1 && Data[oa_oper_type] == OPER_ADD_ITEM)
			{
				client_print_color(Data[oa_player_id], Grey, "%s Too many items in your inventory", CHAT_PREFIX);

				g_block_player[Data[oa_player_id]] = false;
				return SQL_FreeHandle(Query);
			}

			new ip[26], authid[26];
			get_user_ip(Data[oa_player_id], ip, charsmax(ip), .without_port = 1);
			get_user_authid(Data[oa_player_id], authid, charsmax(authid));

			new query[Q_MAX_LEN], data[1]; data[0] = Data[oa_player_id];
			new skin_id = str_to_num(Data[oa_function]);

			new logs[128];
			copy(logs, charsmax(logs), text)

			switch (Data[oa_oper_type])
			{
				case OPER_ADD_ITEM:	formatex(text, charsmax(text), "%s %d", text, skin_id);
				case OPER_REMOVE_ITEM:
				{
					if ( !RemoveNumFromText(text, charsmax(text), skin_id) )
					{
						g_block_player[Data[oa_player_id]] = false;
						return SQL_FreeHandle(Query);
					}
					ReplaceSkin(Data[oa_player_id], skin_id, .replace_skin = 0);
					checkSetStatus(oa_player_id)
				}
			}

			trim(text)

			if (get_pcvar_num(g_Cvars[CVAR_LOG_LEVEL]) >= 3) {
				new n[32]; get_user_name(Data[oa_player_id], n, 31);
				log_to_file("addons/amxmodx/logs/CSR_opers.log", "Player %s[%s][%s] %s item %d | Now [%s] | After [%s]", 
							n, authid, ip, Data[oa_oper_type] == OPER_ADD_ITEM ? "add" : "remove", skin_id, logs, text);
			}
			
			formatex(query, charsmax(query), "UPDATE `%s` SET `%s`='%s' WHERE (`player_id` = '%s' OR `player_ip` = '%s') LIMIT 1",\
			TABLE, Data[oa_table_field], text, authid, ip)

			SQL_ThreadQuery( Data[oa_sqlx], "QueryOperOK", query, data, 1)
		}
	}
	
	return SQL_FreeHandle(Query);
}

public QueryOperOK(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		g_block_player[Data[0]] = false;
		return SQL_Error(Query, Error, Errcode, FailState);
	}
		
	g_block_player[Data[0]] = false;
	return SQL_FreeHandle(Query);
}
stock oper_get_error_string( const OperationReturn: error, str[], len)
{
	if ( error == OperationReturn:OPER_RET_SUCCESS)	return;

	switch (error)
	{
		case OPER_RET_ERROR: formatex(str, len, "[Operation] Invalid Player id or Sql Handle");
		case OPER_PLAYER_NOT_LOAD: formatex(str, len, "[Operation] Player not load from db");
		case OPER_RET_ERROR_MULTI: formatex(str, len, "[Operation] Please try operation again");
	}

}
stock ReplaceSkin(id, const skin_id, const replace_skin = 0)
{
	new data[Items], index = search_array(skin_id);
	
	if(index < 0)	return;
	
	ArrayGetArray(g_Items, index, data)
	new wid = get_weapon_csw(data[ITEM_REPLACE]);
	
	if(g_player_stats[id][WP_SKIN][wid] == skin_id)
		g_player_stats[id][WP_SKIN][wid] = replace_skin;
}

stock RemoveNumFromText( text[], len, const skin_id)
{
	new str[6]; num_to_str(skin_id, str, 5);
	new find, pos, slen = strlen(str);
	do
	{
		find = search_substring(text, str, find);

		if(find == -1)	break;	

		if( (text[find+slen] == ' ' || text[find+slen] == '^0') && ( (text[find-1] == ' ' && find) || !find ) )	pos = 1;
		else	pos = 0;
			
		find++;
	}
	while( !pos )

	if ( find != - 1)
	{
		replace(text[--find], len, str, "")
		replace_all(text,len, "  ", " ")
	}

	return bool:(find != -1);
}
stock search_substring(text[], sub[], start)
{
	new pos = -1;
	
	new tlen = strlen(text);
	new slen = strlen(sub);
	
	new first = -1;
	for(new i = start, j =0; i<tlen; i++)
	{
		if(text[i] == sub[j])
		{
			if(!j)
				first = i;
			j++;
		}
		else	j = 0;
		if(j>=slen)
		{
			pos = first;
			break
		}
	}
	return pos < 0 ? -1 : pos;
}
#if defined DEBUG
	#undef DEBUG
#endif

