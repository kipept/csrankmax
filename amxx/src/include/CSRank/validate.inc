#if defined _validate
	#endinput
#endif
 
#define _validate

#include <amxmodx>
#include <sqlx>

stock bool:isSkinExist( skin_id ) {
	new data[Items], size = ArraySize(g_Items)

	new skin = search_array(skin_id);

	if ( skin < 0 )
		return false;

	for(new i; i< size; i++) {
		ArrayGetArray(g_Items, i, data)

		if (data[ITEM_ID] == skin_id)
			return true;
	}

	return false;
}

stock bool:isSkinInWeaponSlot( skin_id, weapon_id) {
	if ( g_Items == Array:Invalid_Array )	return true; 

	new data[Items], size = ArraySize(g_Items);
	for(new i; i< size; i++)
	{
		ArrayGetArray(g_Items, i, data)
		
		if(data[ITEM_ID] == skin_id && get_weapon_csw(data[ITEM_REPLACE]) == weapon_id)
			return true;
	}
	
	return false
}

stock bool:isFeatureEnabled( id, Array:arr ) {
	new bool:expr = ArraySize(arr) > 0

	if ( !expr ) {
		Print(id, "%L", id, "CSRANK_FEATURE_ENABLED")
		return false;
	}

	return true;
}

stock bool:is_valid_steamid( UserAuthID[] )
{
	if (equali(UserAuthID, "STEAM_ID_LAN") || 
		equali(UserAuthID, "STEAM_ID_PENDING") || 
		equali(UserAuthID, "VALVE_ID_LAN") || 
		equali(UserAuthID,"VALVE_ID_PENDING") || 
		equali(UserAuthID, "STEAM_666:88:666"))
	{
		return false;
	}
	return true;
}

stock bool:has_access(id, flag)
{
	return bool:(get_user_flags(id) & cvar_flags(flag));
}