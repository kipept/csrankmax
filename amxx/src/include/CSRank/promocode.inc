#if defined _promocode_included
	#endinput
#endif
 
#define _promocode_included

#define MAX_PROMO_SHORT_LEN 31
#define MAX_PROMO_LEN 256

#if defined TAG_MODULE
enum _:PromoData
{
	promoCode[32],
	promoType[2],
	promoCount
}

new Array: g_promo;

new g_player_data[MAX_CLIENTS][PlayerData][22]

enum _:PlayerStats
{
	bool:LOAD = 0,
	ID,
	EXP,
	LEVEL,
	MEDAL,
	WP_SKIN[WEAPON_SIZE],
	CASES,
	KEYS,
	COINS,
	TOTAL_COINS,
	WISHES,
	WISH_TIME
}
#endif

#include <amxmodx>

new filePromo[64]

new promoFile[] = "Promocodes.ini"
new promoLog[] = "PromoCode.txt"
new promoTmp[] = "promoTMP.txt";

public initPromo() 
{
	getDirByType(DIR_CONFIG, filePromo, charsmax(filePromo), promoFile)

	if ( !file_exists(filePromo) )
		return PrintMessage("PromoCode .ini file not found in [%s]", filePromo);

	g_promo = ArrayCreate(PromoData);
	return parsePromo( g_promo, filePromo);
}

public savePromo()
{
	new fileTmp[64];
	getDirByType(DIR_CONFIG, fileTmp, charsmax(fileTmp), promoTmp)

	new f = fopen(filePromo, "r");
	new fTemp = fopen(fileTmp, "w");

	if (!f || !fTemp)	return;

	new fileData[256];
	while(!feof(f)) {
		fgets(f, fileData, charsmax(fileData));

		if ( !fileData[0] || fileData[0] == '#' || fileData[0] == ' ') {
			fprintf( fTemp, "%s", fileData)
		}

	}

	fclose(f);

	new data[PromoData], size = ArraySize(g_promo);
	for(new i; i< size; i++) {
		ArrayGetArray(g_promo, i, data)
		fprintf( fTemp, "^"%s^" ^"%s^" ^"%d^"^n", data[promoCode], data[promoType], data[promoCount]);
	}

	fclose(fTemp);

	delete_file(filePromo);
	rename_file(fileTmp, filePromo, 1);
}

public CSRPromoCode(id) 
{
	new arg[36];
	read_argv(1, arg, charsmax(arg));

	if( !strlen(arg) || !ArraySize(g_promo))
		return PLUGIN_HANDLED;

	new index = getPromoByCode(g_promo, arg);

	if ( index < 0 )	return client_print(id, print_console, "Promocode %s already used or not exists", arg);

	AddPromoGift(g_promo, id, index, arg);

	new name[32]; get_user_name(id, name, charsmax(name))
	return ColorChat(0, DontChange, "%s %L",CHAT_PREFIX, id, "CSRANK_PROMO_SUCCESS", name, arg);
}

// Stock **********************************************
stock AddPromoGift( Array:promo, const id, const promo_index, promoCodes[] )
{
	new data[PromoData];
	ArrayGetArray( promo, promo_index, data);

	switch (data[promoType][0]) {
		case 'e': {
			g_player_stats[id][EXP] += data[promoCount];
			g_player_stats[id][LEVEL] = LevelUpdate(id, false)
		}
		case 'c': g_player_stats[id][CASES] += data[promoCount];
		case 'i': AddCoins(id, data[promoCount]);
		case 'k': g_player_stats[id][KEYS] += data[promoCount];
		case 's': scase_oper_player( g_Sqlx, id, data[promoCount], OperationType:OPER_ADD_ITEM, true);
		case 'd': {
			log_amx("ADD item %d", data[promoCount])
			item_add_to_player( g_Sqlx, id, data[promoCount], OperationType:OPER_ADD_ITEM, true);
		}
	}

	addToPromoLog( id, promoCodes,data[promoType][0], data[promoCount] )
	ArrayDeleteItem( promo, promo_index);
}
stock getPromoByCode( Array:promo, const code[])
{
	new data[PromoData];
	new size = ArraySize(promo);

	for( new i; i<size; i++) {
		ArrayGetArray( promo, i, data);

		if ( equal(code, data[promoCode]) )
			return i;
	}

	return -1;
}
stock parsePromo( &Array: promo, const file[])
{
	new f = fopen( file, "rt");

	if ( !f )	return PrintMessage("Can't open file for read [%s]", file);

	#if defined CSRANK_MAX_DEBUG
		log_amx("FilePromo: %s", file)
	#endif

	new filedata[MAX_PROMO_LEN]
	new data[PromoData],dummyCount[6];

	while( !feof(f) ) {

		fgets(f, filedata, MAX_PROMO_LEN);
		trim(filedata);
		if(!filedata[0] || filedata[0] != '"')	continue;

		parse(filedata, data[promoCode], MAX_PROMO_SHORT_LEN, data[promoType], 1, dummyCount, 5 );
		data[promoCount] = str_to_num(dummyCount);

		if ( data[promoCount] == 0)
			PrintMessage("[Promo] %s has NULL count", data[promoCode]);

		ArrayPushArray(promo, data);
	}

	PrintMessage("Load %d ActivePromocode(s)", ArraySize(promo));
	return fclose(f);
}

stock addToPromoLog( const id, code[],  type,  count) {
	new logFile[64];
	getDirByType(DIR_LOG, logFile, charsmax(logFile), promoLog)

	new data[128];
	// Formate [Code] | [Type]::[Count] | Name [SteamID][IP] | Date

	new name[32];
	get_user_name(id, name, charsmax(name))

	new time[32];
	format_time( time, charsmax(time), "%Y-%m-%d %H-%M-%S", -1);

	formatex(data, charsmax(data), " %s | %c:%d | %s [%s][%s] | %s", code, type, count, 
	name, g_player_data[id][PD_AUTHID],g_player_data[id][PD_IP], time)

	log_amx(data);
	write_file( logFile, data, -1);
}

#if defined TAG_MODULE
	#undef PrintMessage
#endif