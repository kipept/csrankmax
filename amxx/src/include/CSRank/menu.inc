#if defined _menu_included
	#endinput
#endif

#define _menu_included

#include <amxmodx>
#include <sqlx>

ShowWishesMenu(id)
{
	new menu, title[150];
	getMenuTitle( title, charsmax(title))

	if (ArraySize(g_wishes) <= 0)
		return client_print_color(id, DontChange, "%s ^3This feature disabled", CHAT_PREFIX)

	new tm = get_systime(0);

	new time[32];
	format_time(time, charsmax(time), "%H[H]:%M[M]:%S[S]", g_player_stats[id][WISH_TIME] - tm)

	formatex(title, charsmax(title),"%s%L^n%L^n%L",title,\
	 id, "CSRANK_WISHES_TITLE", id, "CSRANK_WISHES_COUNT", g_player_stats[id][WISHES], id, "CSRANK_WISHES_ENDTIME", time)
	
	menu = menu_create(title, "HandleWishesMenu")

	formatex(title, charsmax(title), "%s%L", (g_player_stats[id][WISHES] > 0 ) ? "\w" : "\d",  id, "CSRANK_WISHES_OPEN")
	menu_additem(menu, title, "1")

	formatex(title, charsmax(title), "%L^n",id, "CSRANK_WISHES_READ")
	menu_additem(menu, title, "2")

	formatex(title, charsmax(title), "%L",id, "CSRANK_WISHES_BUY", get_pcvar_num(g_Cvars[CVAR_WISH_COST]))
	menu_additem(menu, title, "3")

	return menu_display(id, menu, 0)
}
public HandleWishesMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowShopMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new uid = str_to_num(data);
	
	switch(uid)
	{
		case 1:
		{
			if (g_player_stats[id][WISHES] > 0 )
			{
				new chance, reward[32];
				wishes_give_reward_to_player(g_Sqlx, id, g_wishes, chance, reward, charsmax(reward));

				new name[32];
				get_user_name(id, name, charsmax(name))

				client_print_color(0, DontChange, "%s %L", CHAT_PREFIX, id, "CSRANK_WISHES_REWARD", name, reward)

				g_player_stats[id][WISHES] --;
			}
		}
		case 2: 
		{
			ShowWishesContainer(id);
			return menu_destroy(menu);
		}
		case 3:
		{
			if (g_player_stats[id][COINS] >= get_pcvar_num(g_Cvars[CVAR_WISH_COST]))
			{
				g_player_stats[id][COINS] -= get_pcvar_num(g_Cvars[CVAR_WISH_COST]);

				g_player_stats[id][WISHES] ++;
				client_print_color(id, DontChange, "%s %L", CHAT_PREFIX, id, "CSRANK_WISHES_BUY_ADD", get_pcvar_num(g_Cvars[CVAR_WISH_COST]))
			}
		}
	}
	
	ShowWishesMenu(id)
	return menu_destroy(menu);
}
ShowWishesContainer(id)
{
	new menu, title[150];
	getMenuTitle( title, charsmax(title))

	new tm = get_systime(0);

	new time[32];
	format_time(time, charsmax(time), "%H:%M:%S", g_player_stats[id][WISH_TIME] - tm)

	formatex(title, charsmax(title),"%s%L^n%L^n%L",title,\
	 id, "CSRANK_WISHES_TITLE", id, "CSRANK_WISHES_COUNT", g_player_stats[id][WISHES], id, "CSRANK_WISHES_ENDTIME", time)
	
	menu = menu_create(title, "HandleWishesContainer")

	new size_t = 0;
	new _data[WishesData], data[WishData];

	ArrayGetArray(g_wishes, 0, _data);

	size_t = ArraySize( _data[WISHES_ITEMS] );
	new reward[32], snum[6];
	for(new j = 0; j<size_t; j++)
	{
		ArrayGetArray(_data[WISHES_ITEMS], j, data);

		wishes_get_reward(id, data[WISHES_REWARD], data[WISHES_COUNT], reward, charsmax(reward))
		formatex(title, charsmax(title), "%s \y[%d%%]", reward, data[WISHES_CHANCE]);

		num_to_str(j, snum, charsmax(snum))

		menu_additem(menu, title, snum)
	}

	menu_display(id, menu, 0)
}
public HandleWishesContainer(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowWishesMenu(id)
		return menu_destroy(menu);
	}

	ShowWishesMenu(id)
	return menu_destroy(menu);
}
ShowBuyMenu(id)
{
	new menu, title[150];
	getMenuTitle( title, charsmax(title))

	formatex(title, charsmax(title),"%s%L^n%L^n%L",title,\
	 id, "CSRANK_BUYMENU_TITLE", id, "CSRANK_BUYMENU_CASE", g_player_stats[id][CASES], id, "CSRANK_BUYMENU_KEY", g_player_stats[id][KEYS])
	
	menu = menu_create(title, "HandleBuyMenu")

	formatex(title, charsmax(title), "%s%L", (g_player_stats[id][COINS] >=get_pcvar_num(g_Settings[SET_CASE_PRICE])) ? "\w" : "\d",  id, "CSRANK_BUYMENU_CASES", get_pcvar_num(g_Settings[SET_CASE_PRICE]))
	menu_additem(menu, title, "1")
	
	formatex(title, charsmax(title), "%s%L", (g_player_stats[id][COINS] >= get_pcvar_num(g_Settings[SET_KEY_PRICE])) ? "\w" : "\d",  id, "CSRANK_BUYMENU_KEYS", get_pcvar_num(g_Settings[SET_KEY_PRICE]))
	menu_additem(menu, title, "2")
	
	formatex(title, charsmax(title), "%s%L", (g_player_stats[id][CASES] > 0) ? "\w" : "\d",  id, "CSRANK_SALEC_CASES", get_pcvar_num(g_Settings[SET_CASE_PRICE])*get_pcvar_num(g_Settings[SET_SALE_VALUE])/100)
	menu_additem(menu, title, "3")
	
	formatex(title, charsmax(title), "%s%L", (g_player_stats[id][KEYS] > 0) ? "\w" : "\d",  id, "CSRANK_SALEC_KEYS", get_pcvar_num(g_Settings[SET_KEY_PRICE])*get_pcvar_num(g_Settings[SET_SALE_VALUE])/100)
	menu_additem(menu, title, "4")

	menu_display(id, menu, 0)
}
public HandleBuyMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowShopMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new uid = str_to_num(data);
	
	switch(uid)
	{
		case 1: if (g_player_stats[id][COINS] >=get_pcvar_num(g_Settings[SET_CASE_PRICE]))
		{
			RemoveCoins(id, get_pcvar_num(g_Settings[SET_CASE_PRICE]));
			g_player_stats[id][CASES]++;
		}
		case 2: if (g_player_stats[id][COINS] >=get_pcvar_num(g_Settings[SET_KEY_PRICE]))
		{
			RemoveCoins(id, get_pcvar_num(g_Settings[SET_KEY_PRICE]));
			g_player_stats[id][KEYS]++;
		}
		case 3: if (g_player_stats[id][CASES] > 0)
		{
			g_player_stats[id][CASES]--;
			AddCoins(id,get_pcvar_num(g_Settings[SET_CASE_PRICE])*get_pcvar_num(g_Settings[SET_SALE_VALUE])/100);
		}
		case 4: if (g_player_stats[id][KEYS] > 0)
		{
			g_player_stats[id][KEYS]--;
			AddCoins(id,get_pcvar_num(g_Settings[SET_KEY_PRICE])*get_pcvar_num(g_Settings[SET_SALE_VALUE])/100);
		}
	}
	
	ShowBuyMenu(id)
	return menu_destroy(menu);
}

public SaleMenu(id, szText[], len)
{
	new menu, title[128];
	getMenuTitle( title, charsmax(title))

	formatex(title, charsmax(title),"%s%L",title, id, "CSRANK_SALEMENU_TITLE")
	menu = menu_create(title, "HandleSaleMenu")

	new Array:parsed_items = oper_parse_items_from_text(szText, len);

	new size = ArraySize(parsed_items), data[Items], item_id, temp[16];
	for(new i; i<size; i++)
	{
		item_id = ArrayGetCell(parsed_items, i);

		item_get_info( item_id, data, true);

		formatex(temp, charsmax(temp), "%d", item_id);
		formatex(title, charsmax(title), "%s%s \r[\w%d\r]",  (data[ITEM_CLASS] == 4) ? "\y" : data[ITEM_CLASS] == 3 ? "\r" : data[ITEM_CLASS] == 2 ? "\w" : "\d",data[ITEM_NAME],
		get_item_price(data[ITEM_CLASS]))
		menu_additem(menu, title, temp)
	}

	menu_display(id, menu, 0)
	ArrayDestroy(parsed_items);
}
public HandleSaleMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowShopMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new sid = str_to_num(data);
	
	if (! item_add_to_player(g_Sqlx,id, sid, OperationType:OPER_REMOVE_ITEM) )	return menu_destroy(menu);

	new _data[Items];
	item_get_info( sid, _data);
	AddCoins(id, get_item_price(_data[ITEM_CLASS]));

	client_print_color(id, DontChange, "%s %L",CHAT_PREFIX, id, "CSRANK_SALE_SKIN", _data[ITEM_NAME], get_item_price(_data[ITEM_CLASS]))
	
	ShowShopMenu(id)
	return menu_destroy(menu);
}

public SaleScaseMenu(id, szText[], len)
{
	new menu, title[196];
	getMenuTitle( title, charsmax(title))
	formatex(title, charsmax(title),"%s%L",title,\
	 id, "CSRANK_SHOPMENU_SALESCASE")
	
	menu = menu_create(title, "HandleSaleSCaseMenu")

	new Array: parsed_cases = oper_parse_items_from_text( szText, len);

	new size = ArraySize(parsed_cases), case_id, temp[16], data[CaseData], find = -1;

	if (size == 0)	return	menu_destroy(menu);

	for( new i; i<size; i++)
	{
		case_id = ArrayGetCell(parsed_cases, i);

		find = scase_find_case_by_id(g_array_cases, case_id);
		if (find == -1)	continue;

		ArrayGetArray(g_array_cases, find, data)

		formatex(temp, charsmax(temp), "%d", case_id);
		formatex(title, charsmax(title), "%s%s \r[%d\r]", data[CD_SALE] == -1 ? "\d" : "\y", data[CD_NAME], data[CD_SALE] == -1 ? 0 : data[CD_SALE])
		menu_additem(menu, title, temp)
	}

	ArrayDestroy(parsed_cases);
	return menu_display(id, menu, 0)
}

public HandleSaleSCaseMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowShopMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new sid = str_to_num(data);
	
	new _data[CaseData];
	ArrayGetArray(g_array_cases, scase_find_case_by_id(g_array_cases, sid), _data);

	if (_data[CD_SALE] == -1)	return menu_destroy(menu);
	if (! scase_oper_player(g_Sqlx,id, sid, OperationType:OPER_REMOVE_ITEM) )	return menu_destroy(menu);

	AddCoins(id, _data[CD_SALE]);

	client_print_color(id, DontChange, "%s %L",CHAT_PREFIX, id, "CSRANK_SALE_SCASE", _data[CD_NAME], _data[CD_SALE])
	
	ShowShopMenu(id)
	return menu_destroy(menu);
}

ShowBuyCaseMenu(id)
{
	new menu, title[196];
	getMenuTitle( title, charsmax(title))
	formatex(title, charsmax(title),"%s%L",title,\
	 id, "CSRANK_SHOPMENU_BUYSCASE")
	
	menu = menu_create(title, "HandleBuyCaseMenu")

	new size = ArraySize(g_array_cases), data[CaseData], temp[16];

	if (size == 0)	return menu_destroy(menu);

	for( new i; i<size; i++)
	{
		ArrayGetArray(g_array_cases, i, data)

		formatex(temp, charsmax(temp), "%d", data[CD_INDEX]);
		formatex(title, charsmax(title), "%s%s \r[%d\r]", !data[CD_SHOP] ? "\d" : "\y", data[CD_NAME], data[CD_SHOP])
		menu_additem(menu, title, temp)
	}

	return menu_display(id, menu, 0)
}

public HandleBuyCaseMenu(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		ShowShopMenu(id)
		return menu_destroy(menu);
	}
	
	new data[6], _dummy[1];
	new access, callback;
	menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);

	new sid = str_to_num(data);
	
	new find = scase_find_case_by_id(g_array_cases, sid);

	new _data[CaseData];
	ArrayGetArray(g_array_cases, find, _data);

	if (_data[CD_SHOP] == 0)	return menu_destroy(menu);

	if( g_player_stats[id][COINS] < _data[CD_SHOP] )
		return client_print_color(id, DontChange, "%s %L",CHAT_PREFIX, id, "CSRANK_ENOUGH_COINS") && menu_destroy(menu);

	if (! scase_oper_player(g_Sqlx,id, sid, OperationType:OPER_ADD_ITEM) )	return menu_destroy(menu);

	RemoveCoins(id, _data[CD_SHOP]);

	client_print_color(id, DontChange, "%s %L",CHAT_PREFIX, id, "CSRANK_BUY_SCASE", _data[CD_NAME], _data[CD_SHOP])
	
	ShowShopMenu(id)
	return menu_destroy(menu);
}