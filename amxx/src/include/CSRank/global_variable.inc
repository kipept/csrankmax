#if defined _global_vars_included 
	#endinput
#endif

#define _global_vars_included 

#define get_bit(%1,%2) 		( %1 &   1 << ( %2 & 31 ) )
#define set_bit(%1,%2)	 	( %1 |=  ( 1 << ( %2 & 31 ) ) )
#define clear_bit(%1,%2)	( %1 &= ~( 1 << ( %2 & 31 ) ) )

#define MAX_CLIENTS 32
#define csr_is_valid_player(%0) (1<=%0<=32 && is_user_connected(%0))

//////////////// [SQL]
new const TABLE[] =  "CSRank"
new Handle:g_Sqlx;

///////////////////////////////

/////////// [CONST] /////////////
const WEAPON_SIZE = 31
const PEV_SPEC_TARGET = pev_iuser2

const Q_ITEM_SIZE = 256;
const Q_HUD_LEN = 256;

/* Min Players to take exp */
const _min_players = 2;
/* ini kill exp */
const _expgive_kill = 1

/* SmartCase Const */
const STOCK_GIVE_RANDOM = -2;

new const DEBUG_FILE[] = "addons/amxmodx/logs/CSRANK_DEBUG.log"

new const ITEM_CLASS_NAME [][] = 
{
	"CSRANK_ITEM_NORMAL",
	"CSRANK_ITEM_RARE",
	"CSRANK_ITEM_SPECIAL",
	"CSRANK_ITEM_LEGENDARY"
}

//////////////////////////////////

///////////// [ENUM] //////////////

enum SkinLevel
{
	SL_INVALID = 0,SL_GREY,SL_WHITE,SL_RED,SL_YELLOW
}

enum 
{
	ACR_RET_OK = 0,ACR_RET_ERROR_TYPE,ACR_RET_ERROR_CASE,ACR_RET_ERROR_OPER,ACR_RET_NOTNING
}

enum PlayerData
{
	PD_AUTHID = 0,
	PD_IP,
}

enum _:PlayerStats
{
	bool:LOAD = 0,ID,EXP,LEVEL,MEDAL,WP_SKIN[WEAPON_SIZE],CASES,KEYS,COINS,TOTAL_COINS,WISHES,WISH_TIME,
	SET
}

enum _:Items
{
	ITEM_ID = 0,ITEM_REPLACE[32],ITEM_NAME[64],ITEM_MODEL_V[64],ITEM_MODEL_P[64],ITEM_MODEL_W[64],ITEM_CLASS
}

enum _:WishData
{
    WISHES_CHANCE,WISHES_REWARD,WISHES_COUNT
}

enum _:WishesData
{
    WISHES_DATA_NAME[32],
    Array:WISHES_ITEMS
}

enum Cvars
{
	CVAR_MYSQL_HOST,CVAR_MYSQL_USER,CVAR_MYSQL_PASS,CVAR_MYSQL_DB,CVAR_MYSQL_TABLE,
	CVAR_INFORMER,CVAR_INFORMER_X,CVAR_INFORMER_Y,CVAR_INFORMER_TEXT,
	CVAR_MAIN_FLAG,CVAR_LEVEL_EXP,CVAR_BONUS_HEAD,CVAR_BONUS_KNIFE,
	CVAR_VIP_FLAG,CVAR_VIP_BONUS,CVAR_PRECACHE_ALL,CVAR_LOG_LEVEL,CVAR_DELETE_DAY,
	CVAR_WISH_TIME,CVAR_WISH_ADD,CVAR_WISH_COST
}
enum Settings
{
	SET_VIP_ITEM,SET_VIP_KEY,SET_INI_ITEM_CHANCE,SET_INI_KEY_CHANCE,
	// Del
	SET_LEG_CHANCE,SET_SPEC_CHANCE,SET_RARE_CHANCE,SET_MAXAFFECT_MEDAL,
	SET_INI_ITEMTIME,SET_CHECK_INT,SET_MIN_TIME_ITEM,SET_CASE_PRICE,SET_KEY_PRICE,SET_SALE_VALUE,
	SET_LEG_PRICE,SET_SPEC_PRICE,SET_RARE_PRICE,SET_NORMAL_PRICE,SET_MAXOFFERS_MARKET,SET_CLEAR_LOGS
}
enum 
{
	OP_ADD_ITEM,
	OP_REMOVE_ITEM
}
enum any:mpArray
{
	MP_ID, // offers creator
	MP_NAME[16],
	MP_ITEM, // item_id
	MP_PRICE, // 
	MP_FOR,
	MP_STATUS
}
/* Smart Cases */ 
enum _:CaseData
{
	CD_INDEX = 0,CD_NAME[64],CD_CHANCE,CD_OPEN_KEYS,CD_SHOP,CD_SALE,CD_COLOR[3],Array:CD_ITEMS
}
enum _:PromoData
{
	promoCode[32],promoType[2],promoCount
}
////////////////////////////////

/////////////// [Array] //////////////
new Array: g_wishes;
new Array: g_Items;
new Array:g_exp_level;
new Array:g_marketplace_offers;
new Array: g_array_cases;
new Array: g_promo;
//////////////////////////////////////


new g_player_data[MAX_CLIENTS][PlayerData][22]
new g_player_stats[MAX_CLIENTS][PlayerStats];

new g_Cvars[Cvars];
new g_Settings[Settings];

new bool: g_blocked[33]
new bool:g_take_item[33];

new g_user_kills[33];

new g_marketplace_count[33];
new g_marketplace_item[33];
new g_marketplace_price[33];
new g_marketplace_for[33];
new g_marketplace_time[33];

new g_marketplace_offer_time[33];

new g_vip;

