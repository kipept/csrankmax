Плагин создает и управляет файлами:
logs/CSRank_items.log - отображает выпадение предметов 3 и 4 класса для игроков.
logs/CSRank.log - основные логи плагина.
-------------------------------------------------------------------------------------
Количество предметов в инвентаре каждого игрока ограничено 128 штуками
Max Items count on inventory of each player is 128 

-------------------------------------------------------------------------------------
[Ru]Для #Legendary(4 класс) и #special(3 класс) предметов нужно прописывать все 3 модели(если какой то нет, то заменять стандартной)
в остальных p и w модели нужно прописывать только стандартные, потому что они не будут отображаться.
[Eng]
# When item class < 3, plugin precache only v_model. When item_class >=4: v_model, p_model, w_model;
--------------------------------------------------------------------------------------

[Ru]Сохраняйте все файлы настроек в кодировке uft-8 без boom
[Eng] All files with settings save in encoding uft-8 without boom
--------------------------------------------------------------------------------------
[Ru] Как работает функция получения игроком предмета:
Игрок получает скин с шансом = csrank_init_chance_item(из конфига) + csrank_vip_bonus_item
Иначе(если не получил скин) игрок получает ключ с шансом = csrank_init_chance_key(из конфига) + csrank_vip_bonus_key
иначе он получает кейс.
[Eng]
How work function to add item to player:
Player has chance = csrank_init_chance_item(cfg) + csrank_vip_bonus_item
Key chance =csrank_init_chance_key(cfg) + csrank_vip_bonus_key
Else take case;
--------------------------------------------------------------------------------------
[Ru] Как работает функция получения скина:
Шанс получить легендарный(4 класс) скин=csrank_legendary_chance+медали
Шанс получить эпический(3 класс) скин= csrank_special_chance+медали
Шанс получить рарку(2 класс) скин=csrank_rare_chance+медали
[Eng] How work function take skin:
[1]Chance to take 4 class item=csrank_legendary_chance+medals
Chance to take 3 class item= csrank_special_chance+медали
Chance to take 2 class item= csrank_rare_chance+медали
--------------------------------------------------------------------------------------
[Ru] Получение предмета по времени:
Каждые cvar секунд происходит выдача предметов игрокам;
Чтобы он получил предмет он должен пробыть на текущей карте не менее чем cvar минут
Если он получил предмет, то в течении карты он больше не сможет получить новый.

Шанс получить предмет = ini_cvar + extra_chance;
extra_chance:
Если количество фрагов больше чем смертей и условие рандома random_num(от 1 до frags - death) выполняется то
extra_chance = random(от 1 до (ini_cvar+кол-во медалей));

[En] Take item with time:
Every cvar seconds perform donate items;
To take item player must play on current map not less then cvar minutes
Player that take item can't take again in this map

Chance to take item = ini_cvar + extra_chance;
extra_chance:
if player frags > player deaths and perform random(from 1 to frags-death):
extra+chance = random(from 1 to (ini_cvar)+medals))
--------------------------------------------------------------------------------------